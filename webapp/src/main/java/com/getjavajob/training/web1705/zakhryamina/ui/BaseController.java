package com.getjavajob.training.web1705.zakhryamina.ui;

import com.getjavajob.training.web1705.zakhryamina.common.Account;
import com.getjavajob.training.web1705.zakhryamina.common.Group;
import com.getjavajob.training.web1705.zakhryamina.service.AccountService;
import com.getjavajob.training.web1705.zakhryamina.service.GroupService;
import com.getjavajob.training.web1705.zakhryamina.service.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;

import static com.getjavajob.training.web1705.zakhryamina.dao.utils.PasswordHash.generateHash;

/**
 * Created by Andrey's laptop on 17.11.2017. GJJ-Web
 */
@Controller
@SessionAttributes(value = "accountSession", types = Account.class)
public class BaseController {
    @Autowired
    private AccountService accountService;
    @Autowired
    private GroupService groupService;

    // TODO: 23.11.2017 Возвращать просто строку return:path to page
    @RequestMapping("home")
    public String goHome() {
        return "home";
    }

    @RequestMapping("registration")
    public String goRegistration() {
        return "registration";
    }

    @RequestMapping("login")
    public String goSignIn() {
        return "login";
    }

    @RequestMapping("messages")
    public String goMessages() {
        return "messages";
    }

    @ModelAttribute("Account")
    public Account createModel() {
        return new Account();
    }

    @RequestMapping(value = "do-login", method = RequestMethod.POST)
    public ModelAndView signIn(@RequestParam String email, @RequestParam String password,
                               @RequestParam(value = "remember-me", required = false) String rememberMe,
                               HttpServletResponse httpServletResponse)
            throws CustomGenericException {
        Account account;
        ModelAndView modelAndView = new ModelAndView("redirect:login");
        try {
            account = accountService.getAccountByEmail(email);
            modelAndView.addObject("accountSession", accountService.getAccountByEmail(email));
        } catch (ServiceException e) {
            // TODO: 23.11.2017 Throw exception, spring mvc exception handler
            throw new CustomGenericException(e, "Failed to obtain account from session");
        }
        if (account == null || !account.getPassword().equals(generateHash(password))) {
//            redirectAttributes.addFlashAttribute("reason", "Invalid pair of email and password");
            return modelAndView;
        } else {
            if (rememberMe != null) {
                httpServletResponse.addCookie(new Cookie("accountEmail", account.getEmail()));
                httpServletResponse.addCookie(new Cookie("accountPassword", account.getPassword()));
            }
            modelAndView.setViewName("redirect:home");
            return modelAndView;
        }
    }

    @RequestMapping("signOut")
    public ModelAndView signOut(HttpSession session, HttpServletResponse httpServletResponse) {
        session.invalidate();
        Cookie emailKiller = new Cookie("accountEmail", null);
        Cookie passwordKiller = new Cookie("accountPassword", null);
        emailKiller.setMaxAge(0);
        passwordKiller.setMaxAge(0);
        httpServletResponse.addCookie(emailKiller);
        httpServletResponse.addCookie(passwordKiller);
        return new ModelAndView("redirect:login");
    }

    @RequestMapping(value = "do-register", method = RequestMethod.POST)
    public String doRegister(@ModelAttribute("Account") Account account,
                             @RequestParam("password1") String repeatedPassword,
                             HttpSession session) throws CustomGenericException {
        if (!account.getPassword().equals(repeatedPassword)) {
//            redirectAttributes.addFlashAttribute("reason", "Password doesn't match");
            return "redirect:registration";
        }
        try {
            if (accountService.getAccountByEmail(account.getEmail()) != null) {
                // TODO: 23.11.2017 flash scope spring mvc
//                redirectAttributes.addFlashAttribute("reason", "The account with the same email already exist in database!");
            }
        } catch (Exception e) {
            try {
                accountService.insertEntry(account);
            } catch (ServiceException e1) {
                throw new CustomGenericException(e1, "Failed to create account");
            }
            session.setAttribute("accountSession", account);
            return "redirect:home";
        }
        return "redirect:registration";
    }

    @RequestMapping(value = "show_friends", method = RequestMethod.GET)
    public ModelAndView showFriends(HttpSession session) throws CustomGenericException {
        List<Account> accountsFriends;
        List<Account> accountsFriendsRequested;
        List<Account> accountsFriendsRequests;
        try {
            accountsFriends = accountService.getFriendsList((Account) session.getAttribute("accountSession"));
            accountsFriendsRequested = accountService.getFollowedByList((Account) session.getAttribute("accountSession"));
            accountsFriendsRequests = accountService.getFollowersList((Account) session.getAttribute("accountSession"));
        } catch (ServiceException e) {
            throw new CustomGenericException(e, "Failed to get friends/followers of account");
        }
        ModelAndView modelAndView = new ModelAndView("friends");
        modelAndView.addObject("friends", accountsFriends);
        modelAndView.addObject("friendsRequested", accountsFriendsRequested);
        modelAndView.addObject("friendsRequests", accountsFriendsRequests);
        return modelAndView;
    }

    @RequestMapping(value = "show_profile", method = RequestMethod.GET)
    public ModelAndView showProfile(@RequestParam String email) throws CustomGenericException {
        Account account;
        try {
            account = accountService.getAccountByEmail(email);
        } catch (ServiceException e) {
            throw new CustomGenericException(e, "Failed to get account by email");
        }
        ModelAndView modelAndView = new ModelAndView("profileInfo");
        modelAndView.addObject("friend", account);
        return modelAndView;
    }

    @RequestMapping(value = "set_friendship", method = RequestMethod.GET)
    public String getFriendship(@RequestParam String email, @RequestParam String status, HttpSession session) throws CustomGenericException {
        try {
            Account accountFriend = accountService.getAccountByEmail(email);
            Account account = (Account) session.getAttribute("accountSession");
            if (status.equals("accept")) {
                accountService.acceptFriendRequest(account, accountFriend);
            } else if (status.equals("decline")) {
                accountService.declineFriendRequest(account, accountFriend);
            }
        } catch (ServiceException e) {
            throw new CustomGenericException(e, "Failed to update status for friendship");
        }
        return "redirect:friends";
    }

    @RequestMapping(value = "show_groups", method = RequestMethod.GET)
    public ModelAndView showGroups(HttpSession session) throws CustomGenericException {
        List<Group> groups;
        groups = groupService.getGroupsOfAccount((Account) session.getAttribute("accountSession"));
        ModelAndView modelAndView = new ModelAndView("groups");
        modelAndView.addObject("groups", groups);
        return modelAndView;
    }

    @RequestMapping(value = "show_search")
    public ModelAndView showSearchResults(@RequestParam("search-by-pattern") String searchPattern) throws CustomGenericException {
        ModelAndView modelAndView;
        try {
            modelAndView = new ModelAndView("SearchResults");
            modelAndView.addObject("searchResultsGroup", groupService.searchGroupsByName(searchPattern.toLowerCase()));
            modelAndView.addObject("searchResultsAccount", accountService.searchForAccountsByStr(searchPattern.toLowerCase()));
        } catch (ServiceException e) {
            throw new CustomGenericException(e, "Failed to get search results");
        }
        return modelAndView;
    }
}