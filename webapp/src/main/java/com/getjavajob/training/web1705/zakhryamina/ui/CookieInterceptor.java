package com.getjavajob.training.web1705.zakhryamina.ui;

import com.getjavajob.training.web1705.zakhryamina.common.Account;
import com.getjavajob.training.web1705.zakhryamina.service.AccountService;
import com.getjavajob.training.web1705.zakhryamina.service.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Andrey's laptop on 22.10.2017. GJJ-Web
 */
public class CookieInterceptor extends HandlerInterceptorAdapter {
    @Autowired
    private AccountService accountService;

    public void setAccountService(AccountService accountService) {
        this.accountService = accountService;
    }

    private final List<String> allowLinks = Arrays.asList(
            "/login", "/registration", "/signIn", "/do-login", "/do-register");

    @Override
    public boolean preHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o)
            throws IOException, ServletException {
        HttpSession session = httpServletRequest.getSession();
        Cookie[] cookies = httpServletRequest.getCookies();
        Cookie accountEmailCookie = findCookie(cookies, "accountEmail");
        Cookie accountPasswordCookie = findCookie(cookies, "accountPassword");
        Account accountSession = (Account) session.getAttribute("accountSession");
        String requestURI = httpServletRequest.getRequestURI();
        String params = httpServletRequest.getQueryString();
        if (params != null) {
            requestURI += "?" + params;
        }
        if (allowLinks.contains(requestURI)) {
            return true;
        }
        if (accountSession != null) {
            try {
                if (accountService.isLogged(accountSession.getEmail(), accountSession.getPassword())) {
                    return true;
                } else {
                    httpServletResponse.sendRedirect("/login");
                }
                return false;
            } catch (ServiceException e) {
                throw new ServletException(e);
            }
        } else {
            if (cookies != null) {
                if (accountEmailCookie != null && accountPasswordCookie != null) {
                    try {
                        if (accountService.isLogged(accountEmailCookie.getValue(), accountPasswordCookie.getValue())) {
                            Account account = accountService.getAccountByEmail(accountEmailCookie.getValue());
                            session.setAttribute("accountSession", account);
                            return true;
                        }
                    } catch (ServiceException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        httpServletResponse.sendRedirect("/login");
        return false;
    }

    private Cookie findCookie(Cookie[] cookies, String cookieName) {
        if (cookies == null || cookieName == null) {
            return null;
        } else {
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals(cookieName)) {
                    return cookie;
                }
            }
            return null;
        }
    }
}