package com.getjavajob.training.web1705.zakhryamina.ui;

import com.getjavajob.training.web1705.zakhryamina.common.Account;
import com.getjavajob.training.web1705.zakhryamina.service.AccountService;
import com.getjavajob.training.web1705.zakhryamina.service.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Date;

import static com.getjavajob.training.web1705.zakhryamina.dao.utils.PasswordHash.generateHash;

/**
 * Created by Andrey's laptop on 18.11.2017. GJJ-Web
 */
@Controller
@SessionAttributes(value = "accountSession", types = Account.class)
public class SettingsController {
    @Autowired
    private AccountService accountService;

    @RequestMapping(value = "settings")
    public ModelAndView goSettings() {
        return new ModelAndView("settings");
    }

    @ModelAttribute("account")
    public Account setModel() {
        return new Account();
    }

    @PostMapping("setSettings")
    public ModelAndView setSettings(@RequestParam(required = false, name = "old_password") String oldPass,
                                    @RequestParam(required = false, name = "phones_list") String[] newPersonalPhones,
                                    @RequestParam(required = false, name = "work_phones_list") String[] newWorkPhones,
                                    @RequestParam(required = false, value = "new_password") String newPass,
                                    @RequestParam(required = false, value = "new_password_again") String newPassAgain,
                                    @RequestParam(required = false, value = "file") MultipartFile filePart,
                                    @ModelAttribute("account") Account account,
                                    HttpSession session) {
        Account oldAccount = null;
        ModelAndView modelAndView = new ModelAndView("redirect:settings");
        try {
            Account sessionAccount = (Account) session.getAttribute("accountSession");
            oldAccount = accountService.getAccountByEmail(sessionAccount.getEmail());
        } catch (ServiceException e) {
            e.printStackTrace();
        }
        if (oldPass.isEmpty() || !(oldAccount != null && oldAccount.getPassword().equals(generateHash(oldPass)))) {
            return modelAndView;
        }
        String newName = account.getName();
        if (newName != null && !newName.isEmpty()) {
            oldAccount.setName(newName);
        }
        String newFamilyName = account.getSurname();
        if (newFamilyName != null && !newFamilyName.isEmpty()) {
            oldAccount.setSurname(newFamilyName);
        }
        String newMiddleName = account.getMiddleName();
        if (newMiddleName != null && !newMiddleName.isEmpty()) {
            oldAccount.setMiddleName(newMiddleName);
        }
        Date newBirthday = account.getBirthday();
        if (newBirthday != null) {
            oldAccount.setBirthday(newBirthday);
        }
        if (newPersonalPhones != null) {
            oldAccount.setPersonalPhones(Arrays.asList(newPersonalPhones));
        } else {
            oldAccount.setPersonalPhones(null);
        }
        if (newWorkPhones != null) {
            oldAccount.setWorkPhones(Arrays.asList(newWorkPhones));
        } else {
            oldAccount.setWorkPhones(null);
        }
        String newEmail = account.getEmail();
        if (newEmail != null && !newEmail.isEmpty()) {
            oldAccount.setEmail(newEmail);
        }
        String newIcq = account.getIcq();
        if (newIcq != null && !newIcq.isEmpty()) {
            oldAccount.setIcq(newIcq);
        }
        String newSkype = account.getSkype();
        if (newSkype != null && !newSkype.isEmpty()) {
            oldAccount.setSkype(newSkype);
        }
        String newAddInfo = account.getSupplementInfo();
        if (newAddInfo != null && !newAddInfo.isEmpty()) {
            oldAccount.setSupplementInfo(newAddInfo);
        }
        if (newPass != null && !newPass.isEmpty() && newPass.equals(newPassAgain)) {
            oldAccount.setPassword(generateHash(newPass));
        }

        if (filePart != null && filePart.getSize() != 0) {
            try (InputStream fileContent = filePart.getInputStream()) {
                byte[] pic = extractImage(fileContent);
                if (pic.length != 0) {
                    oldAccount.setAvatar(pic);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        try {
            accountService.modifyEntry(oldAccount);
            modelAndView.addObject("accountSession", oldAccount);
        } catch (ServiceException e) {
            e.printStackTrace();
        }
        modelAndView.setViewName("redirect:home");
        return modelAndView;
    }

    private byte[] extractImage(InputStream inputStream) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024];
        int read;
        while ((read = inputStream.read(buffer, 0, buffer.length)) != -1) {
            baos.write(buffer, 0, read);
        }
        baos.flush();
        return baos.toByteArray();
    }
}