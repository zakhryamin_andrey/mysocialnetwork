package com.getjavajob.training.web1705.zakhryamina.ui;

import com.getjavajob.training.web1705.zakhryamina.service.AccountService;
import com.getjavajob.training.web1705.zakhryamina.service.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Created by Andrey's laptop on 18.11.2017. GJJ-Web
 */
@Controller
public class PictureController {
    @Autowired
    private AccountService accountService;

    @ResponseBody
    @RequestMapping(value = "showAvatar", method = RequestMethod.GET, produces = MediaType.IMAGE_JPEG_VALUE)
    public byte[] showAvatar(@RequestParam("id") int id, HttpServletRequest httpServletRequest) throws IOException {
        byte[] friendAvatar = null;
        try {
            friendAvatar = accountService.getAvatarById(id);
            if (friendAvatar == null) {
                Path fileLocation = Paths.get(httpServletRequest.getServletContext().getRealPath("/img/" + "default_acc1.jpg"));
                friendAvatar = Files.readAllBytes(fileLocation);
            }
        } catch (ServiceException e) {
            e.printStackTrace();
        }
        return friendAvatar;
        // TODO: 23.11.2017 return bytearray, responsebody annotation
    }
}