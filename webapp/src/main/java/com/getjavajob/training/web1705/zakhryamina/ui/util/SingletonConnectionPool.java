package com.getjavajob.training.web1705.zakhryamina.ui.util;

import com.getjavajob.training.web1705.zakhryamina.dao.TransactionalBlockingConnectionPool;

/**
 * Created by Andrey's laptop on 24.10.2017. GJJ-Web
 */
public class SingletonConnectionPool {
    private static volatile TransactionalBlockingConnectionPool connectionPool;

    private SingletonConnectionPool() {
    }

    public static TransactionalBlockingConnectionPool getPoolInstance() {
        if (connectionPool == null) {
            synchronized (SingletonConnectionPool.class) {
                if (connectionPool == null) {
                    connectionPool = new TransactionalBlockingConnectionPool();
                }
            }
        }
        return connectionPool;
    }
}