package com.getjavajob.training.web1705.zakhryamina.ui;

/**
 * Created by Andrey's laptop on 24.11.2017. GJJ-Web
 */
public class CustomGenericException extends Exception {
    private static final long serialVersionUID = 1L;

    public CustomGenericException(Throwable e, String errMsg) {
        super("Error by the following reason:" + errMsg, e);
    }

    public CustomGenericException(String errMsg) {
        super("Custom constructor:" + errMsg);
    }
}