package com.getjavajob.training.web1705.zakhryamina.ui;

import com.getjavajob.training.web1705.zakhryamina.common.Account;
import com.getjavajob.training.web1705.zakhryamina.common.DBAccessable;
import com.getjavajob.training.web1705.zakhryamina.common.Group;
import com.getjavajob.training.web1705.zakhryamina.service.AccountService;
import com.getjavajob.training.web1705.zakhryamina.service.GroupService;
import com.getjavajob.training.web1705.zakhryamina.service.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Andrey's laptop on 23.11.2017. GJJ-Web
 */
@RestController
public class AjaxController {
    @Autowired
    private AccountService accountService;
    @Autowired
    private GroupService groupService;

    @RequestMapping("/searchByPattern")
    public List<SearchEntity> searchAccounts(final @RequestParam("pattern") String pattern) throws ServiceException {
        List<SearchEntity> entities = new LinkedList<>();
        List<Account> accounts = accountService.searchForAccountsByStr(pattern);
        List<Group> groups = groupService.searchGroupsByName(pattern);
        for (Account account : accounts) {
            entities.add(new SearchEntity(account));
        }
        for (Group group : groups) {
            entities.add(new SearchEntity(group));
        }
        return entities;
    }

    private static class SearchEntity {
        private String primaryName;
        private String secondaryName;
        private String type;
        private long id;

        public SearchEntity(DBAccessable baseEntity) {
            if (baseEntity instanceof Account) {
                Account account = (Account) baseEntity;
                primaryName = account.getSurname();
                secondaryName = account.getName();
                type = "account";
                id = account.getId();
            } else if (baseEntity instanceof Group) {
                Group group = (Group) baseEntity;
                primaryName = group.getName();
                secondaryName = "";
                type = "group";
                id = group.getId();
            }
        }

        public String getPrimaryName() {
            return primaryName;
        }

        public String getSecondaryName() {
            return secondaryName;
        }

        public String getType() {
            return type;
        }

        public long getId() {
            return id;
        }
    }
}