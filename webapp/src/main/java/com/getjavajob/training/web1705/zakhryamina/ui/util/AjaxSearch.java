package com.getjavajob.training.web1705.zakhryamina.ui.util;

import com.getjavajob.training.web1705.zakhryamina.common.Account;
import com.getjavajob.training.web1705.zakhryamina.common.Group;
import com.getjavajob.training.web1705.zakhryamina.service.AccountService;
import com.getjavajob.training.web1705.zakhryamina.service.GroupService;
import com.getjavajob.training.web1705.zakhryamina.service.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Andrey's laptop on 27.11.2017. GJJ-Web
 */
@RestController
public class AjaxSearch {
    @Autowired
    private AccountService accountService;

    @Autowired
    private GroupService groupService;

    @RequestMapping("/searchByStr")
    public List SearchForEntries(final @RequestParam("pattern") String pattern) {
        List<Group> groupList = null;
        List<Account> accountList = null;
        List<AutoComplete> result = new ArrayList<>();
        try {
            groupList = groupService.searchGroupsByName(pattern.toLowerCase());
            accountList = accountService.searchForAccountsByStr(pattern.toLowerCase());
        } catch (ServiceException e) {
            e.printStackTrace();
        }
        if (groupList != null) {
            for (Group group : groupList) {
                result.add(new AutoComplete(group.getName(), "Groups"));
            }
        }
        if (accountList != null) {
            for (Account account : accountList) {
                result.add(new AutoComplete(account.getName() + " " + account.getSurname(), "Accounts"));
            }
        }
        return result;
    }
}

class AutoComplete {
    private final String label;
    private final String category;

    public AutoComplete(String label, String category) {
        super();
        this.label = label;
        this.category = category;
    }

    public final String getLabel() {
        return this.label;
    }

    public final String getCategory() {
        return this.category;
    }
}
