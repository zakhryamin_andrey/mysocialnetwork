package com.getjavajob.training.web1705.zakhryamina.ui;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created by Andrey's laptop on 24.11.2017. GJJ-Web
 */
@ControllerAdvice
public class GlobalExceptionController {
    @ExceptionHandler(Exception.class)
    public ModelAndView handleAllException(Exception ex) {
        ModelAndView model = new ModelAndView("genericError");
        model.addObject("exc", ex);
        return model;
    }

    @ExceptionHandler(CustomGenericException.class)
    public ModelAndView handleCustomException(Exception ex) {
        System.out.println("----Caught FileNotFoundException----");
        ModelAndView mav = new ModelAndView();
        mav.addObject("exc", ex);
        mav.setViewName("genericError");
        return mav;
    }
}
