$(document).ready(
    function () {
        var searchValue;
        $("#search-id").autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: 'searchByPattern',
                    data: {
                        pattern: request.term
                    },
                    success: function (data) {
                        response($.map(data, function (entry, i) {
                            searchValue = entry.primaryName;
                            return {
                                value: searchValue,
                                label: entry.secondaryName + ' ' + searchValue + ' : ' + entry.type
                            }
                        }));
                    }
                });
            },
            select: function (event, ui) {
                window.location.replace("show_search?search-by-pattern=" + ui.item.value);
            },
            minLength: 1
        });
    }
);