$(document).ready(function () {
    var maxFields = 10; //maximum skills boxes allowed
    var personalPhonesWrapper = $(".input_personal_phones_wrap"); //Fields phonesWrapper
    var workPhonesWrapper = $(".input_work_phones_wrap"); //Fields phonesWrapper
    var addPersonalButton = $(".add_personal_phone_button"); //Add button class
    var addWorkButton = $(".add_work_phone_button"); //Add button class
    var saveSettings = $(".settings_save_button");
    var appendPersonalText = '' +
        '<div><input class="form-control"  name="phones_list" placeholder="Add phone" type="text" maxlength="15"/>' +
        '<div class="input-group-btn">' +
        '<a type="button" class="remove_personal_phones_button btn btn-danger">' +
        '<span class="glyphicon glyphicon-minus"></span>' +
        '</a></div><br></div>';
    var appendWorkText = '' +
        '<div><input class="form-control"  name="work_phones_list" placeholder="Add phone" type="text" maxlength="15"/>' +
        '<div class="input-group-btn">' +
        '<a type="button" class="remove_work_phones_button btn btn-danger">' +
        '<span class="glyphicon glyphicon-minus"></span>' +
        '</a></div><br></div>';

    var x = 1; //initlal text box count
    $(addPersonalButton).click(function (e) { //on add input button click
        e.preventDefault();
        if (x < maxFields) { //max input box allowed
            x++; //text box increment
            $(personalPhonesWrapper).append(appendPersonalText); //add input box
        }
    });
    $(addWorkButton).click(function (e) { //on add input button click
        e.preventDefault();
        if (x < maxFields) { //max input box allowed
            x++; //text box increment
            $(workPhonesWrapper).append(appendWorkText); //add input box

        }
    });

    $(personalPhonesWrapper).on("click", ".remove_personal_phones_button", function (e) { //user click on remove phone
        e.preventDefault();
        $(this).parent('div').parent('div').remove();
        x--;
    });
    $(workPhonesWrapper).on("click", ".remove_work_phones_button", function (e) { //user click on remove phone
        e.preventDefault();
        $(this).parent('div').parent('div').remove();
        x--;
    });

    var currentForm;
    $(function () {
        $("#confirm").dialog({
            autoOpen: false,
            modal: true,
            width: 300,
            height: 250,
            resizable: false,
            buttons: {
                'Save settings': function () {
                    $(this).dialog('close');
                    currentForm.submit();
                },
                'Cancel': function () {
                    $(this).dialog('close');
                }
            },
            show: {
                effect: "highlight",
                duration: 1500
            },
            hide: {
                effect: "fade",
                duration: 1000
            }
        });
    });

    $(saveSettings).click(function () {
        currentForm = $(this).closest('form'); // Set form to submit from dialog
        var personalPhones = document.getElementsByName("phones_list");
        var workPhones = document.getElementsByName("work_phones_list");
        for (i = 0; i < personalPhones.length; i++) {
            console.log("phone.value" + personalPhones[i].value);
            if (!validate(personalPhones[i].value)) {
                return false;
            }
        }
        for (i = 0; i < workPhones.length; i++) {
            console.log("phone.value" + workPhones[i].value);
            if (!validate(workPhones[i].value)) {
                return false;
            }
        }
        $("#confirm").dialog('open');
        return false;
    });

    /*$('#personal_phone_val').blur(function (e) {
     if (validate('personal_phone_val')) {
     $('#spnPhoneStatus').html('Valid');
     $('#spnPhoneStatus').css('color', 'green');
     }
     else {
     $('#spnPhoneStatus').html('Invalid');
     $('#spnPhoneStatus').css('color', 'red');
     }
     });*/

    function validate(arg) {
        var reg = /^[0-9-+]+$/;
        if (!reg.test(arg) || arg.length == 0) {
            alert(arg + " phone is invalid (empty or contain letters or include ()%*&!^)");
            return false;
        } else {
            return true;
        }
    }
});