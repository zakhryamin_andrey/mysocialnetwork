<%@include file="common/header.jsp"%>
<body>
<%@include file="common/MenuPanel.jsp"%>
<c:set var="account" value='${sessionScope.get("accountSession")}'/>
<div class="container" style="margin-top:30px;margin-right: 30px">
    <div class="row profile">
        <div class="col-md-4">
                <!-- SIDEBAR USERPIC -->
            <div class="w3-image" style="margin-bottom: 20px">
                <img src="${pageContext.request.contextPath}/showAvatar?id=${account.id}"
                     style="width: auto; height: auto; max-width: 300px; max-height: 350px">
            </div>
                <!-- END SIDEBAR USERPIC -->

                <!-- SIDEBAR BUTTONS -->
                <div class="profile-userbuttons">
                    <button type="button" class="btn btn-success btn-sm">Some button</button>
                    <button type="button" class="btn btn-success btn-sm">Some button</button>
                </div>
                <!-- END SIDEBAR BUTTONS -->
        </div>
        <div class="col-md-6">
            <div class="profile-content">
                <!-- SIDEBAR USER TITLE -->
                <div class="profile-user">
                    <div class="profile-user-name">
                        <c:out value="${account.name}"/>
                        <c:choose>
                            <c:when test='${account.middleName != null}'>
                                <c:out value='${account.middleName}'/>
                            </c:when>
                        </c:choose>
                        <c:out value='${account.surname}'/>
                    </div>
                    <div class="profile-user-contact">
                        <c:choose>
                            <c:when test='${account.registrationDate != null}'>
                                <strong>Registration date:</strong> <c:out value='${account.registrationDate}'/>
                                <br>
                            </c:when>
                        </c:choose>
                        <c:choose>
                            <c:when test='${account.birthday != null}'>
                                <strong>Birthday:</strong> <c:out value='${account.birthday}'/>
                                <br>
                            </c:when>
                        </c:choose>
                        <c:choose>
                            <c:when test='${account.email != null}'>
                                <strong>Email:</strong> <c:out value='${account.email}'/>
                                <br>
                            </c:when>
                        </c:choose>
                        <c:choose>
                            <c:when test='${account.convertPersonalPhonesToString() != null}'>
                                <strong>Personal phone:</strong> <c:out
                                    value='${account.convertPersonalPhonesToString()}'/>
                                <br>
                            </c:when>
                        </c:choose>
                        <c:choose>
                            <c:when test='${account.convertWorkPhonesToString() != null}'>
                                <strong>Work phone:</strong> <c:out value='${account.convertWorkPhonesToString()}'/>
                                <br>
                            </c:when>
                        </c:choose>
                        <c:choose>
                            <c:when test='${account.icq != null}'>
                                <strong>ICQ:</strong> <c:out value='${account.icq}'/>
                                <br>
                            </c:when>
                        </c:choose>
                        <c:choose>
                            <c:when test='${account.skype != null}'>
                                <strong>Skype:</strong> <c:out value='${account.skype}'/>
                                <br>
                            </c:when>
                        </c:choose>
                        <c:choose>
                            <c:when test='${account.supplementInfo != null}'>
                                <strong>Additional information:</strong>
                                <br>
                                <c:out value='${account.supplementInfo}'/>
                                <br>
                            </c:when>
                        </c:choose>
                    </div>
                </div>
                <!-- END SIDEBAR USER TITLE -->
            </div>
        </div>
    </div>
</div>
<br>
<br>

<%@include file="common/footer.jsp" %>
</body>
