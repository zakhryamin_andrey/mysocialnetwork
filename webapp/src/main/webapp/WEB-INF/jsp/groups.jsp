<%@include file="common/header.jsp" %>
<body>
<%@include file="common/MenuPanel.jsp" %>

<div class="container" style="margin-top: 30px; margin-right: 30px">
    <div class="row">
        <h1>
            My Groups
        </h1>
    </div>
    <div class="row">
        <c:forEach var="group" items="${groups}">
        <div class="col-sm-3">
            <div class="card">
                <canvas class="header-bg" width="250" height="70"></canvas>
                <div class="avatar">
                    <img src="${pageContext.request.contextPath}/img/default_group.jpg"
                         alt=""/>
                </div>
                <div class="content">
                    <p>${group.getName()}
                        <br>
                        by ${group.getOwner().getName()} ${group.getOwner().getSurname()}
                    </p>
                    <p>
                        <button type="button" class="btn btn-default">Show group</button>
                    </p>
                </div>
            </div>
        </div>
        </c:forEach>
    </div>
</div>

<%@include file="common/footer.jsp" %>
</body>
