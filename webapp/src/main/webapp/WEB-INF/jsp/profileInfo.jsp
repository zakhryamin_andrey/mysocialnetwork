<%@include file="common/header.jsp" %>
<body>
<%@include file="common/MenuPanel.jsp" %>
<c:set var="friendAccount" value="${friend}" scope="request"/>
<div class="container" style="margin-top:30px; margin-right: 30px">
    <div class="row profile">
        <div class="col-md-4">
            <!-- SIDEBAR USERPIC -->
            <div class="w3-image" style="margin-bottom: 20px">
                <img src="${pageContext.request.contextPath}/showAvatar?id=${friendAccount.getId()}"
                     style="width: auto; height: auto; max-width: 300px; max-height: 350px">
            </div>
            <!-- END SIDEBAR USERPIC -->

            <!-- SIDEBAR BUTTONS -->
            <div class="w3-bar-item">
                <a href='<c:url value="/set_friendship?email=${friendAccount.getEmail()}"/>'
                   class="w3-button w3-black">Add to friends</a>
            </div>
            <!-- END SIDEBAR BUTTONS -->
        </div>
        <div class="col-md-6">
            <div class="profile-content">
                <!-- SIDEBAR USER TITLE -->
                <div class="profile-user">
                    <div class="profile-user-name">
                        <c:out value='${friendAccount.getName()}'/>
                        <c:choose>
                            <c:when test='${friendAccount.getMiddleName() != null}'>
                                <c:out value='${friendAccount.getMiddleName()}'/>
                            </c:when>
                        </c:choose>
                        <c:out value='${friendAccount.getSurname()}'/>
                    </div>
                    <div class="profile-user-contact">
                        <c:choose>
                            <c:when test='${friendAccount.getRegistrationDate() != null}'>
                                <strong>Registration date:</strong> <c:out
                                    value='${friendAccount.getRegistrationDate()}'/>
                                <br>
                            </c:when>
                        </c:choose>
                        <c:choose>
                            <c:when test='${friendAccount.getBirthday() != null}'>
                                <strong>Birthday:</strong> <c:out value='${friendAccount.getBirthday()}'/>
                                <br>
                            </c:when>
                        </c:choose>
                        <c:choose>
                            <c:when test='${friendAccount.getEmail() != null}'>
                                <strong>Email:</strong> <c:out value='${friendAccount.getEmail()}'/>
                                <br>
                            </c:when>
                        </c:choose>
                        <c:choose>
                            <c:when test='${friendAccount.convertPersonalPhonesToString() != null}'>
                                <strong>Personal phone:</strong> <c:out
                                    value='${friendAccount.convertPersonalPhonesToString()}'/>
                                <br>
                            </c:when>
                        </c:choose>
                        <c:choose>
                            <c:when test='${friendAccount.convertWorkPhonesToString() != null}'>
                                <strong>Work phone:</strong> <c:out
                                    value='${friendAccount.convertWorkPhonesToString()}'/>
                                <br>
                            </c:when>
                        </c:choose>
                        <c:choose>
                            <c:when test='${friendAccount.getIcq() != null}'>
                                <strong>ICQ:</strong> <c:out value='${friendAccount.getIcq()}'/>
                                <br>
                            </c:when>
                        </c:choose>
                        <c:choose>
                            <c:when test='${friendAccount.getSkype() != null}'>
                                <strong>Skype:</strong> <c:out value='${friendAccount.getSkype()}'/>
                                <br>
                            </c:when>
                        </c:choose>
                        <c:choose>
                            <c:when test='${friendAccount.getSupplementInfo() != null}'>
                                <strong>Additional information:</strong>
                                <br>
                                <c:out value='${friendAccount.getSupplementInfo()}'/>
                                <br>
                            </c:when>
                        </c:choose>
                    </div>
                </div>
                <!-- END SIDEBAR USER TITLE -->
            </div>
        </div>
    </div>
</div>
<br>
<br>

<%@include file="common/footer.jsp" %>
</body>
