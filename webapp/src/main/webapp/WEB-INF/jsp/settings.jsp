<%@include file="common/header.jsp" %>
<body>
<%@include file="common/MenuPanel.jsp" %>

<div class="container" style="margin-left:250px; margin-bottom: 20px;margin-top: 20px">
    <c:set var="myAccount" value="${accountSession}" scope="session"/>
    <div class="row">
        <h1>
            Change profile information
        </h1>
    </div>

    <form:form class="form-horizontal" action="${pageContext.request.contextPath}/setSettings" method="POST"
               enctype="multipart/form-data" modelAttribute="account">
        <div class="form-group">
            <label for="input-image" class="col-md-2 control-label">Upload profile image</label>
            <div class="col-md-4">
                <input type="file" class="form-control-file" id="input-image" name="file" aria-describedby="fileHelp"/>
                <small id="fileHelp" class="form-text text-muted">
                    Please, choose the file with image extension
                </small>
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-2 control-label">First name</label>
            <div class="col-md-4">
                <form:input path="name" class="form-control" maxlength="30" name="name" placeholder="First name"
                            value="${myAccount.name}" type="text"/>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2 control-label">Middle name</label>
            <div class="col-md-4">
                <form:input path="middleName" class="form-control" maxlength="30" name="middle_name"
                            value="${myAccount.middleName}"
                            placeholder="Middle name" type="text"/>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2 control-label">Family name</label>
            <div class="col-md-4">
                <form:input path="surname" class="form-control" maxlength="30" name="family_name"
                            placeholder="Family name"
                            value="${myAccount.surname}" type="text"/>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2 control-label">Birthday</label>
            <div class="col-md-4">
                <form:input path="birthday" type="date" class="form-control" id="birthday" name="birthday"
                            placeholder="Birthday"
                            value="${myAccount.birthday}"/>
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-2 control-label">Email</label>
            <div class="col-md-4">
                <form:input path="email" class="form-control" name="email" placeholder="email"
                            value="${myAccount.email}"
                            type="text"/>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2 control-label">ICQ</label>
            <div class="col-md-4">
                <form:input path="icq" class="form-control" maxlength="9" name="icq" placeholder="icq"
                            value="${myAccount.icq}" type="text"/>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2 control-label">Skype</label>
            <div class="col-md-4">
                <form:input path="skype" class="form-control" name="skype" placeholder="Skype"
                            value="${myAccount.skype}"
                            type="text"/>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2 control-label">Additional information</label>
            <div class="col-md-4">
                <form:textarea path="supplementInfo" class="form-control" name="add_info" rows="3"></form:textarea>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2 control-label">Personal phones</label>
            <div class="col-md-4 form-inline">
                <div class="input_personal_phones_wrap input-group">
                    <div>
                        <a class="add_personal_phone_button btn btn-success">Add More Personal Phones</a>
                    </div>
                    <br>
                    <c:forEach var='phone' items='${sessionScope.get("accountSession").getPersonalPhones()}'>
                        <div>
                            <input class="form-control" name="phones_list" value="${phone}" placeholder="Add phone"
                                   type="text" maxlength="15"/>
                            <div class="input-group-btn">
                                <a type="button" class="remove_personal_phones_button btn btn-danger">
                                    <span class="glyphicon glyphicon-minus"></span>
                                </a>
                            </div>
                            <br>
                        </div>
                    </c:forEach>
                </div>
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-2 control-label">Corporate phones</label>
            <div class="col-md-4 form-inline">
                <div class="input_work_phones_wrap input-group">
                    <div>
                        <a class="add_work_phone_button btn btn-success">Add More Corporate Phones</a>
                    </div>
                    <br>
                    <c:forEach var='workPhone' items='${sessionScope.get("accountSession").getWorkPhones()}'>
                        <div>
                            <input class="form-control" name="work_phones_list" value="${workPhone}"
                                   placeholder="Add phone"
                                   type="text" maxlength="15"/>
                            <div class="input-group-btn">
                                <a type="button" class="remove_work_phones_button btn btn-danger">
                                    <span class="glyphicon glyphicon-minus"></span>
                                </a>
                            </div>
                            <br>
                        </div>
                    </c:forEach>
                </div>
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-2 control-label">New password</label>
            <div class="col-md-4">
                <input class="form-control" name="new_password" placeholder="New password" type="password"/>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2 control-label">New password (again)</label>
            <div class="col-md-4">
                <input class="form-control" name="new_password_again" placeholder="New password (again)"
                       type="password"/>
            </div>
        </div>

        <div class="form-group required">
            <label class="col-md-2 control-label">Old password</label>
            <div class="col-md-4">
                <input class="form-control" name="old_password" placeholder="Old password" required="required"
                       type="password"/>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="settings_save_button btn btn-primary">Change information</button>
            </div>
        </div>
    </form:form>
</div>
<div id="confirm" title="">
    <p>Are you sure you want to proceed and save changes?</p>
</div>
<%@include file="common/footer.jsp" %>
</body>