<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@include file="common/header.jsp" %>
<body>
<div class="container">

    <form:form class="form-signin" method="POST" action="/do-login" modelAttribute="account">
        <h2 class="form-signin-heading">Please sign in</h2>
        <label for="input-email" class="sr-only">Email address</label>
        <input type="email" id="input-email" class="form-control" name="email" placeholder="Email address" required
               autofocus>
        <label for="input-password" class="sr-only">Password</label>
        <input type="password" id="input-password" class="form-control" name="password" placeholder="Password" required>
        <div class="checkbox">
            <label>
                <input type="checkbox" value="remember-me" name="remember-me"> Remember me
            </label>
        </div>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
        <c:choose>
            <c:when test='${reason != null}'>
                <div class="alert alert-danger">
                    <c:out value='${reason}'/>
                </div>
            </c:when>
        </c:choose>
    </form:form>

    <form class="form-register" method="POST" action="<c:url value="/registration"/>">
        <h3 class="form-register-heading">Or register</h3>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Register</button>
    </form>

</div> <!-- /container -->
</body>
</html>