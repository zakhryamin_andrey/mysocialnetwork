<%@include file="common/header.jsp" %>
<body>
<h1>Error : ${exc.message}</h1>
<c:forEach items="${exc.stackTrace}" var="st">
    ${st}
</c:forEach>
</body>
<%@include file="common/footer.jsp" %>