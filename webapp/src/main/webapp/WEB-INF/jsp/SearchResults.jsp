<%@include file="common/header.jsp" %>
<body>
<%@include file="common/MenuPanel.jsp" %>

<div class="container" style="margin-right:30px; margin-top: 30px;margin-bottom: 30px">
    <div class="w3-container">
        <h5>Friends</h5>
        <table class="w3-table w3-striped w3-bordered w3-border w3-hoverable w3-white myTable">
            <tr class="w3-teal">
                <td>Name
                </td>
                <td>Surname
                </td>
                <td>E-mail address
                </td>
                <td>Link to details
                </td>
            </tr>
            <c:forEach var="friendAccount" items="${searchResultsAccount}">
                <tr>
                    <td>${friendAccount.getName()}
                    </td>
                    <td>${friendAccount.getSurname()}
                    </td>
                    <td>${friendAccount.getEmail()}
                    </td>
                    <td>
                        <a href='<c:url value="/show_profile?email=${friendAccount.getEmail()}"/>'
                           class="w3-button w3-black">Details</a>
                    </td>
                </tr>
            </c:forEach>
        </table>
        <h5>Groups</h5>
        <table class="w3-table w3-striped w3-bordered w3-border w3-hoverable w3-white">
            <tr class="w3-teal">
                <td>Group Name
                </td>
                <td>Supplement information
                </td>
                <td>Group creator
                </td>
                <td>Link to details
                </td>
            </tr>
            <c:forEach var="groupEntry" items="${searchResultsGroup}">
                <tr>
                    <td>${groupEntry.getName()}
                    </td>
                    <td>${groupEntry.getSupplementInfo()}
                    </td>
                    <td>${groupEntry.getOwner().getSurname()}
                    </td>
                    <td>
                        <a href='<c:url value="/show_profile?email=${groupEntry.getOwner().getEmail()}"/>'
                           class="w3-button w3-black">Details</a>
                    </td>
                </tr>
            </c:forEach>
        </table>
    </div>
</div>

<%@include file="common/footer.jsp" %>
</body>