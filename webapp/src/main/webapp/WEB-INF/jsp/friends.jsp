<%@include file="common/header.jsp" %>

<body>
<%@include file="common/MenuPanel.jsp" %>

<div class="container" style="margin-top: 30px; margin-right: 30px">
    <div id="tabs">
        <ul>
            <li><a href="#tabs-1">Friends</a></li>
            <li><a href="#tabs-2">Followers</a></li>
            <li><a href="#tabs-3">Accounts followed by me</a></li>
        </ul>
        <div id="tabs-1" class="w3-container">
            <c:forEach var="friendAccount" items="${friends}">
                <div class="w3-grayscale">
                    <div class="w3-col l3 m6 w3-margin-bottom" style="margin-right: 40px">
                        <div class="w3-card">
                            <img src="${pageContext.request.contextPath}/showAvatar?id=${friendAccount.getId()}"
                                 alt="${friendAccount.name}" style="width:100%">
                            <div class="w3-container">
                                <h3>${friendAccount.name} ${friendAccount.surname}</h3>
                                <p class="w3-opacity">Friend</p>
                                <p>${friendAccount.supplementInfo}</p>
                                <p>
                                    <a href='<c:url value="/show_profile?email=${friendAccount.getEmail()}"/>'
                                       class="w3-button w3-light-grey w3-block"><i class="fa fa-envelope"></i>
                                        Show profile
                                    </a>
                                    <a href='<c:url value="/set_friendship?email=${friendAccount.getEmail()}&status=decline"/>>'
                                       class="w3-button w3-light-grey w3-block"><i class="fa fa-envelope"></i>
                                        Remove from friends
                                    </a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </c:forEach>
        </div>
        <div id="tabs-2">
            <c:forEach var="followedByMe" items="${friendsRequested}">
                <div class="w3-grayscale">
                    <div class="w3-col l3 m6 w3-margin-bottom" style="margin-right: 40px">
                        <div class="w3-card">
                            <img src="${pageContext.request.contextPath}/showAvatar?id=${followedByMe.getId()}"
                                 alt="${followedByMe.name}" style="width:100%">
                            <div class="w3-container">
                                <h3>${followedByMe.name} ${follower.surname}</h3>
                                <p class="w3-opacity">Follower</p>
                                <p>${followedByMe.supplementInfo}</p>
                                <p>
                                    <a href='<c:url value="/show_profile?email=${followedByMe.getEmail()}"/>'
                                       class="w3-button w3-light-grey w3-block"><i class="fa fa-envelope"></i>
                                        Show profile
                                    </a>
                                    <a href='<c:url value="/set_friendship?email=${followedByMe.getEmail()}&status=decline"/>>'
                                       class="w3-button w3-light-grey w3-block"><i class="fa fa-envelope"></i>
                                        Decline my friend request
                                    </a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </c:forEach>
        </div>
        <div id="tabs-3">
            <c:forEach var="follower" items="${friendsRequests}">
                <div class="w3-grayscale">
                    <div class="w3-col l3 m6 w3-margin-bottom" style="margin-right: 40px">
                        <div class="w3-card">
                            <img src="${pageContext.request.contextPath}/showAvatar?id=${follower.getId()}"
                                 alt="${follower.name}" style="width:100%">
                            <div class="w3-container">
                                <h3>${follower.name} ${follower.surname}</h3>
                                <p class="w3-opacity">Follower</p>
                                <p>${follower.supplementInfo}</p>
                                <p>
                                    <a href='<c:url value="/show_profile?email=${follower.getEmail()}"/>'
                                       class="w3-button w3-light-grey w3-block"><i class="fa fa-envelope"></i>
                                        Show profile
                                    </a>
                                    <a href='<c:url value="/set_friendship?email=${follower.getEmail()}&status=accept"/>>'
                                       class="w3-button w3-light-grey w3-block"><i class="fa fa-envelope"></i>
                                        Add to friends
                                    </a><a
                                        href='<c:url value="/set_friendship?email=${follower.getEmail()}&status=decline"/>>'
                                        class="w3-button w3-light-grey w3-block"><i class="fa fa-envelope"></i>
                                    Remove from friends
                                </a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </c:forEach>
        </div>
    </div>
</div>

<%--<%@include file="common/footer.jsp" %>--%>
</body>