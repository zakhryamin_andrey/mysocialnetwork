<footer class="w3-container w3-teal w3-center w3-bottom" style="height: 60px">
    <h5 style="font-size: 12px; margin-top: 0.5em; margin-bottom: 0.1em">Developed by A. Zakhryamin. Find me on social
        media.</h5>
    <i class="fa fa-facebook-official w3-hover-opacity"></i>
    <i class="fa fa-instagram w3-hover-opacity"></i>
    <i class="fa fa-snapchat w3-hover-opacity"></i>
    <i class="fa fa-pinterest-p w3-hover-opacity"></i>
    <i class="fa fa-twitter w3-hover-opacity"></i>
    <i class="fa fa-linkedin w3-hover-opacity"></i>
    <i class="fa fa-vk w3-hover-opacity"></i>
    <h5 style="font-size: 12px">Powered by <a href="http://www.getjavajob.com" target="_blank">getjavajob.ru</a></h5>
</footer>