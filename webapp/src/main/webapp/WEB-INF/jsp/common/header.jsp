<%@page contentType="text/html; charset=UTF-8" language="java" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<html>
<head>
    <title>Antisocial</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet"
          href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/themes/smoothness/jquery-ui.css"/>
    <link rel="stylesheet" href="<%=request.getContextPath()%>/bootstrap/css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="<%=request.getContextPath()%>/css/4w3css.css" type="text/css">
    <link rel="stylesheet" href="<%=request.getContextPath()%>/css/settings-page-style.css" type="text/css">
    <link rel="stylesheet" href="<%=request.getContextPath()%>/css/register-page-style.css" type="text/css">
    <link rel="stylesheet" href="<%=request.getContextPath()%>/css/friends-page-style.css" type="text/css">
    <link rel="stylesheet" href="<c:url value="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"/>">

    <style>
        body, h1, h2, h3, h4, h5, h6 {
            font-family: "Montserrat", sans-serif
        }

        .w3-row-padding img {
            margin-bottom: 12px
        }

        /* Set the width of the sidebar to 120px */
        .w3-sidebar {
            width: 120px;
            background: #222;
        }

        /* Add a left margin to the "page content" that matches the width of the sidebar (120px) */
        #main {
            margin-left: 120px
        }

        /* Remove margins from "page content" on small screens */
        @media only screen and (max-width: 600px) {
            #main {
                margin-left: 0
            }
        }

        .ui-autocomplete-category {
            font-weight: bold;
            padding: .2em .4em;
            margin: .8em 0 .2em;
            line-height: 1.5;
        }
    </style>
    <script src="${pageContext.request.contextPath}/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="${pageContext.request.contextPath}/js/jquery-3.2.1.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/dynamicSettingsValidator.js"></script>
    <script src="${pageContext.request.contextPath}/js/dynamicSearch.js"></script>
    <%--<script src="${pageContext.request.contextPath}/js/search-script.js"></script>--%>
    <script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
        $(function () {
            $("#tabs").tabs();
        });
    </script>

    <!-- Top container -->
    <div class="w3-top">
        <div class="w3-bar w3-theme-d2 w3-left-align w3-black" style="height: 40px">
            <a href="${pageContext.request.contextPath}/home"
               class="w3-bar-item w3-button w3-teal w3-hide-small"><i class="fa fa-home w3-margin-right"></i>AntiSocial</a>
            <a href="#" class="w3-bar-item w3-button w3-hide-small w3-padding-large w3-hover-white" title="News"><i
                    class="fa fa-globe"></i></a>
            <a href="${pageContext.request.contextPath}/settings"
               class="w3-bar-item w3-button w3-hide-small w3-padding-large w3-hover-white"
               title="Account Settings"><i class="fa fa-cog fa-user"></i></a>
            <div class="w3-dropdown-hover w3-hide-small">
                <button class="w3-button w3-padding-large" title="Notifications"><i class="fa fa-bell"></i><span
                        class="w3-badge w3-right w3-small w3-green">3</span></button>
                <div class="w3-dropdown-content w3-card-4 w3-bar-block" style="width:300px">
                    <a href="#" class="w3-bar-item w3-button">Notification 1</a>
                    <a href="#" class="w3-bar-item w3-button">Notification 2</a>
                    <a href="#" class="w3-bar-item w3-button">Notification 3</a>
                </div>
            </div>
            <form class="w3-bar-item w3-button w3-hide-small 3-hover-teal" method="post" action="show_search">
                <input id="search-id" style="height: 25px; width: 300px; margin-left: 20px; font-size: 14px"
                       class="w3-input w3-text-black w3-border fa fa-search"
                       type="search" placeholder="Search for groups and friends" name="search-by-pattern" max="50">

            </form>
            <a href="${pageContext.request.contextPath}/signOut"
               class="w3-bar-item w3-button w3-hide-small w3-right w3-hover-white" title="Log out">
                <img src="${pageContext.request.contextPath}/img/log_out.jpg" class="w3-circle"
                     style="height:30px;width:30px" alt="Avatar"></a>
        </div>
    </div>
</head>