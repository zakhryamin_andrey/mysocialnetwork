<c:set var="myAccount" value="${accountSession}" scope="session"/>
<nav class="w3-sidebar w3-bar-block w3-small w3-hide-small w3-center">
    <!-- Avatar image in top left corner -->
    <img src="${pageContext.request.contextPath}/showAvatar?id=${myAccount.getId()}" style="width:100%">
    <a class="w3-bar-item w3-black">
        <i class="fa w3-xxlarge"></i>
        <h6>Welcome, <strong><c:out value='${myAccount.getName()}'/> !</strong></h6>
    </a>
    <a href="${pageContext.request.contextPath}/show_friends"
       class="w3-bar-item w3-button w3-padding-large w3-hover-black">
        <i class="fa fa-user w3-xxlarge"></i>
        <p>FRIENDS</p>
    </a>
    <a href="${pageContext.request.contextPath}/show_groups"
       class="w3-bar-item w3-button w3-padding-large w3-hover-black">
        <i class="fa fa-users w3-xxlarge"></i>
        <p>GROUPS</p>
    </a>
    <a href="${pageContext.request.contextPath}/messages" class="w3-bar-item w3-button w3-padding-large w3-hover-black">
        <i class="fa fa-envelope w3-xxlarge"></i>
        <p>MESSAGES</p>
    </a>
</nav>
<!-- /.navbar -->