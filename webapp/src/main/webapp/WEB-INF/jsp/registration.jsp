<%@include file="common/header.jsp" %>
<body>
<form:form action="do-register" method="post" role="form" class="form-horizontal" style="margin-left:300px"
           commandName="Account">
    <%--<input type='hidden' name='csrfmiddlewaretoken' value='brGfMU16YyyG2QEcpLqhb3Zh8AvkYkJt'/>--%>
    <div class="form-group required">
        <label class="col-md-2 control-label">Last name</label>
        <div class="col-md-4">
            <form:input path="surname" class="form-control" id="id_last_name" maxlength="30" name="last_name"
                        placeholder="Last name" required="required" title="" type="text"/>
        </div>
    </div>
    <div class="form-group required">
        <label class="col-md-2 control-label">First name</label>
        <div class="col-md-4">
            <form:input path="name" class="form-control" id="id_first_name" maxlength="30" name="first_name"
                        placeholder="First name" required="required" type="text"/>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-2 control-label">Middle name</label>
        <div class="col-md-4">
            <form:input path="middleName" class="form-control" id="id_middle_name" maxlength="30" name="middle_name"
                        placeholder="Middle name" type="text"/>
        </div>
    </div>
    <div class="form-group required">
        <label class="col-md-2 control-label">E-mail</label>
        <div class="col-md-4">
            <form:input path="email" class="form-control" id="id_email" name="email" placeholder="E-mail"
                        required="required" type="email"/>
        </div>
    </div>
    <div class="form-group required">
        <label class="col-md-2 control-label">Password</label>
        <div class="col-md-4">
            <form:input path="password" class="form-control" id="id_password" name="password" placeholder="Password"
                        required="required" type="password"/>
        </div>
    </div>
    <div class="form-group required">
        <label class="col-md-2 control-label">Password (again)</label>
        <div class="col-md-4">
            <input class="form-control" id="id_password1" name="password1"
                   placeholder="Password (again)" required="required" type="password"/>
        </div>
    </div>

    <div class="form-group required">
        <label class="col-md-2 control-label"></label>
        <div class="col-md-4">
            <div class="checkbox">
                <label>
                    <input required="required" type="checkbox"/>I have read and agree to the Terms of Service
                </label>
            </div>
        </div>
    </div>

    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-primary">
                <span class="glyphicon glyphicon-star"></span> Register
            </button>
        </div>
    </div>
</form:form>
<%@include file="common/footer.jsp" %>
</body>
