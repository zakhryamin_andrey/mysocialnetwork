package com.getjavajob.training.web1705.zakhryamina.common;

import javax.persistence.*;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Andrey's laptop on 05.09.2017. GJJ-Web
 */
@Entity
@Table(name = "GROUPS_TBL", schema = "SOCIAL_NETWORK_DB")
public class Group implements DBAccessable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(name = "GROUP_NAME")
    private String name;
    @ManyToMany(mappedBy = "accountGroups")
    private List<Account> members;
    @OneToOne
    @JoinColumn(name = "CREATOR_ID", referencedColumnName = "ID", nullable = false)
    private Account owner;
    @Column(name = "ADD_INFO")
    private String supplementInfo;
    private Date registrationDate;
    @Column(name = "STATUS")
    private boolean activeStatus;
    private byte[] avatar;

    public Group(int id, String name, Account owner) {
        this.id = id;
        this.name = name;
        this.owner = owner;
        this.members = new LinkedList<>();
    }

    public Group(int id) {
        this.id = id;
        this.members = new LinkedList<>();
    }

    public Group() {
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public void setId(int id) {
        this.id = id;
    }

    @Override
    public byte[] getAvatar() {
        return avatar;
    }

    @Override
    public void setAvatar(byte[] avatar) {
        this.avatar = avatar;
    }

    @Override
    public Date getRegistrationDate() {
        return registrationDate;
    }

    @Override
    public void setRegistrationDate(Date registrationDate) {
        this.registrationDate = registrationDate;
    }

    @Override
    public boolean isActiveStatus() {
        return activeStatus;
    }

    @Override
    public void setActiveStatus(boolean activeStatus) {
        this.activeStatus = activeStatus;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Account> getMembers() {
        return members;
    }

    public void addMember(Account account) {
        members.add(account);
    }

    public void setMembers(List<Account> members) {
        this.members = members;
    }

    public Account getOwner() {
        return owner;
    }

    public void setOwner(Account owner) {
        this.owner = owner;
    }

    public String getSupplementInfo() {
        return supplementInfo;
    }

    public void setSupplementInfo(String supplementInfo) {
        this.supplementInfo = supplementInfo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Group group = (Group) o;

        return name != null ? name.equals(group.name) : group.name == null;
    }

    @Override
    public int hashCode() {
        return name != null ? name.hashCode() : 0;
    }
}