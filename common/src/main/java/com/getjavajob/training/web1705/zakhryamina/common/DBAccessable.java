package com.getjavajob.training.web1705.zakhryamina.common;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by Andrey's laptop on 11.09.2017. GJJ-Web
 */
public interface DBAccessable extends Serializable {
    int getId();

    void setId(int id);

    Date getRegistrationDate();

    void setRegistrationDate(Date date);

    boolean isActiveStatus();

    void setActiveStatus(boolean status);

    byte[] getAvatar();

    void setAvatar(byte[] avatar);
}