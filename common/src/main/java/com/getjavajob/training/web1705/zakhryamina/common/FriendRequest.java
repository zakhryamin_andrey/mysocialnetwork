package com.getjavajob.training.web1705.zakhryamina.common;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Andrey's laptop on 30.11.2017. GJJ-Web
 */
@Entity
@NamedNativeQueries({
        @NamedNativeQuery(name = "findAllAccountRequests",
                query = "SELECT * FROM FRIENDS_TBL WHERE ACCOUNT_ID = :from OR FRIEND_ID = :to")
})
@Table(name = "friends_tbl", schema = "social_network_db")
public class FriendRequest implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @ManyToOne
    @JoinColumn(name = "ACCOUNT_ID")
    private Account fromAccount;
    @Id
    @ManyToOne
    @JoinColumn(name = "FRIEND_ID")
    private Account toAccount;
    @Column(name = "STATUS")
    private int status;

    public FriendRequest() {
    }

    public FriendRequest(Account fromAccount, Account toAccount) {
        setToAccount(toAccount);
        setFromAccount(fromAccount);
    }

    public FriendRequest(Account fromAccount, Account toAccount, int status) {
        this.fromAccount = fromAccount;
        this.toAccount = toAccount;
        this.status = status;
    }

    public Account getFromAccount() {
        return fromAccount;
    }

    public void setFromAccount(Account fromAccount) {
        this.fromAccount = fromAccount;
        if (!fromAccount.getRequestsFromMe().contains(this)) {
            fromAccount.addRequestsFromMe(this);
        }
    }

    public Account getToAccount() {
        return toAccount;
    }

    public void setToAccount(Account toAccount) {
        this.toAccount = toAccount;
        if (!toAccount.getRequestsToMe().contains(this)) {
            toAccount.addRequestsToMe(this);
        }
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "FriendRequest{" +
                "fromAccount=" + (fromAccount != null ? String.valueOf(fromAccount.getId()) : null) +
                ", toAccount=" + (toAccount != null ? String.valueOf(toAccount.getId()) : null) +
                ", accepted=" + status +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        FriendRequest request = (FriendRequest) o;

        if (status != request.status) {
            return false;
        }
        if (fromAccount != null ? !fromAccount.equals(request.fromAccount) : request.fromAccount != null) {
            return false;
        }
        return toAccount != null ? toAccount.equals(request.toAccount) : request.toAccount == null;

    }

    @Override
    public int hashCode() {
        int result = fromAccount != null ? fromAccount.hashCode() : 0;
        result = 31 * result + (toAccount != null ? toAccount.hashCode() : 0);
        result = 31 * result + status;
        return result;
    }
}