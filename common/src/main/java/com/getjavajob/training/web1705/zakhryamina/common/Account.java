package com.getjavajob.training.web1705.zakhryamina.common;

import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Andrey's laptop on 05.09.2017. GJJ-Web
 */
// TODO: 23.09.2017 Primary key phones table change to separate field , autoinc
@Entity
@Table(name = "ACCOUNTS_TBL", schema = "SOCIAL_NETWORK_DB")
public class Account implements DBAccessable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(nullable = false)
    private String name;
    @Column(nullable = false)
    private String surname;
    @Column(name = "MIDDLE_NAME")
    private String middleName;
    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "PHONES_TBL", schema = "SOCIAL_NETWORK_DB", joinColumns = @JoinColumn(name = "ACCOUNT_ID"))
    @Where(clause = "IS_PRIVATE=TRUE")
    @Column(name = "PHONE")
    private List<String> personalPhones;
    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "PHONES_TBL", schema = "SOCIAL_NETWORK_DB", joinColumns = @JoinColumn(name = "ACCOUNT_ID"))
    @Where(clause = "IS_PRIVATE=FALSE")
    @Column(name = "PHONE")
    private List<String> workPhones;
    @Column(nullable = false)
    private String email;
    @Temporal(TemporalType.DATE)
    private Date birthday;
    @Column(name = "REGISTRATION_DATE", columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    private Date registrationDate;
    @Column(name = "PERSONAL_ADDRESS")
    private String personalAddress;
    @Column(name = "WORK_ADDRESS")
    private String workAddress;
    private String icq;
    private String skype;
    @Column(name = "PASS")
    private String password;
    @Column(name = "SUPPLEMENT_INFO")
    private String supplementInfo;
    @Column(name = "STATUS")
    private boolean activeStatus;
    @OneToMany(mappedBy = "fromAccount")
    private List<FriendRequest> requestsFromMe;
    @OneToMany(mappedBy = "toAccount")
    private List<FriendRequest> requestsToMe;
    private byte[] avatar;
    @ManyToMany
    @JoinTable(name = "members_tbl", schema = "social_network_db",
            joinColumns = @JoinColumn(name = "MEMBER_ID", referencedColumnName = "ID"),
            inverseJoinColumns = @JoinColumn(name = "GROUP_ID", referencedColumnName = "ID"))
    private List<Group> accountGroups;

    public Account() {
        personalPhones = new ArrayList<>();
        workPhones = new ArrayList<>();
        requestsFromMe = new ArrayList<>();
        requestsToMe = new ArrayList<>();
        accountGroups = new ArrayList<>();
    }

    public Account(int id, String name, String surname, String email) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.personalPhones = new ArrayList<>();
        this.workPhones = new ArrayList<>();
        this.activeStatus = true;
        this.personalAddress = null;
    }

    public Account(String name, String surname, String email, String password) {
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.personalPhones = new ArrayList<>();
        this.workPhones = new ArrayList<>();
        this.activeStatus = true;
        this.password = password;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Date getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(Date registrationDate) {
        this.registrationDate = registrationDate;
    }

    public byte[] getAvatar() {
        return avatar;
    }

    public void setAvatar(byte[] avatar) {
        this.avatar = avatar;
    }

    @Override
    public void setActiveStatus(boolean activeStatus) {
        this.activeStatus = activeStatus;
    }

    @Override
    public boolean isActiveStatus() {
        return activeStatus;
    }

    @Override
    public void setId(int id) {
        this.id = id;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public void setWorkPhones(List<String> workPhones) {
        this.workPhones = workPhones;
    }

    public void setPersonalPhones(List<String> personalPhones) {
        this.personalPhones = personalPhones;
    }

    public void addPersonalPhone(String personalPhone) {
        this.personalPhones.add(personalPhone);
    }

    public void clearPersonalPhones() {
        this.personalPhones.clear();
    }

    public void clearWorkPhones() {
        this.workPhones.clear();
    }

    public void setWorkPhone(String workPhone) {
        this.workPhones.add(workPhone);
    }

    public String convertPersonalPhonesToString() {
        return showPhones(this.personalPhones);
    }

    public String convertWorkPhonesToString() {
        return showPhones(this.workPhones);
    }

    public void clearAvatar() {
        this.avatar = null;
    }


    private String showPhones(List<String> phones) {
        if (phones != null) {
            StringBuilder phonesToString = new StringBuilder();
            for (String phone : phones) {
                phonesToString.append(phone + "; ");
            }
            if (phonesToString.length() > 2) {
                return phonesToString.toString().substring(0, phonesToString.length() - 2);
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    public void setAccountGroups(List<Group> accountGroups) {
        this.accountGroups = accountGroups;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public void setPersonalAddress(String personalAddress) {
        this.personalAddress = personalAddress;
    }

    public void setWorkAddress(String workAddress) {
        this.workAddress = workAddress;
    }

    public void setIcq(String icq) {
        this.icq = icq;
    }

    public void setSkype(String skype) {
        this.skype = skype;
    }

    public void setSupplementInfo(String supplementInfo) {
        this.supplementInfo = supplementInfo;
    }

    @Override
    public int getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getMiddleName() {
        return middleName;
    }

    public List<String> getPersonalPhones() {
        return personalPhones;
    }

    public List<String> getWorkPhones() {
        return workPhones;
    }

    public Date getBirthday() {
        return birthday;
    }

    public String getPersonalAddress() {
        return personalAddress;
    }

    public String getWorkAddress() {
        return workAddress;
    }

    public String getIcq() {
        return icq;
    }

    public List<Group> getAccountGroups() {
        return accountGroups;
    }

    public List<FriendRequest> getRequestsFromMe() {
        return requestsFromMe;
    }

    public String getSkype() {
        return skype;
    }

    public List<FriendRequest> getRequestsToMe() {
        return requestsToMe;
    }

    public String getSupplementInfo() {
        return supplementInfo;
    }

    public List<Account> getFriends() {
        List<Account> friends = new LinkedList<>();
        List<FriendRequest> requests = requestsToMe;
        requests.addAll(requestsFromMe);
        for (FriendRequest request : requests) {
            if (request.getFromAccount().equals(this) && request.getStatus() == 1) {
                friends.add(request.getToAccount());
            } else if (request.getToAccount().equals(this) && request.getStatus() == 1) {
                friends.add(request.getFromAccount());
            }
        }
        return friends;
    }

    public void setRequestsFromMe(List<FriendRequest> requestsFromMe) {
        this.requestsFromMe = requestsFromMe;
    }

    public void setRequestsToMe(List<FriendRequest> requestsToMe) {
        this.requestsToMe = requestsToMe;
    }

    public List<Account> getFollowers() {
        List<Account> followers = new LinkedList<>();
        for (FriendRequest request : requestsToMe) {
            if (request.getToAccount().equals(this) && request.getStatus() != 1) {
                followers.add(request.getFromAccount());
            }
        }
        return followers;
    }

    public List<Account> getFollowedByAccount() {
        List<Account> followed = new LinkedList<>();
        for (FriendRequest request : requestsFromMe) {
            if (request.getFromAccount().equals(this) && request.getStatus() != 1) {
                followed.add(request.getToAccount());
            }
        }
        return followed;
    }

    public void addRequestsFromMe(FriendRequest request) {
        this.requestsFromMe.add(request);
        if (request.getFromAccount() != this) {
            request.setFromAccount(this);
        }
    }

    public void addRequestsToMe(FriendRequest request) {
        this.requestsToMe.add(request);
        if (request.getToAccount() != this) {
            request.setToAccount(this);
        }
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Account account = (Account) o;

        return email.equals(account.email);
    }

    @Override
    public int hashCode() {
        return email.hashCode();
    }

}