# Project "Social Network"  
  
**Functionality:**  
  
+ registration  
+ AJAX search autocomplete      
+ display profile  
+ edit profile  
+ upload profile picture  
+ users export to / import from XML    
+ adding friends  
  
**Tools:**  
JDK 7, Spring 4, JPA 2 / Hibernate 4, Twitter Bootstrap 3, W3school-css, jQuery 2, JUnit 4, Mockito, Maven 3, XStream, Git / Bitbucket, Tomcat 7, MySQL, IntelliJIDEA 15.  

Social Network, September 2017  
_    
**Zakhryamin Andrey**  
Training getJavaJob  
http://www.getjavajob.com