package com.getjavajob.training.web1705.zakhryamina.service;

import com.getjavajob.training.web1705.zakhryamina.common.Account;
import com.getjavajob.training.web1705.zakhryamina.dao.AccountDAO;


/**
 * Created by Andrey's laptop on 20.09.2017. GJJ-Web
 */

public class AccountServiceTest {
    private AccountService accountService;
    private AccountDAO accountDAO;
    private Account testAccount;

    /*@Before
    public void initializeTestData() {
        this.accountDAO = mock(AccountDAO.class);
        this.accountService = new AccountService(accountDAO);
        this.testAccount = new Account(1, "Genadiy", "Zuganov", "gzu@gmail.com");
    }

    @Test
    public void createAccountTest() throws DaoException, ServiceException {
        Account accToInsert = new Account(1, "Genadiy", "Zuganov", "gzu@gmail.com");
        when(accountDAO.insert(testAccount)).thenReturn(accToInsert);
        assertEquals(accountService.createAccount(testAccount), accToInsert);
    }

    @Test
    public void deleteAccountTest() throws DaoException, ServiceException {
        accountService.deleteEntry(testAccount.getId());
        verify(accountDAO).deleteById(testAccount.getId());
    }

    @Test
    public void modifyAccountTest() throws DaoException, ServiceException {
        accountService.modifyEntryById(testAccount.getId(), "Name", "Valentin");
        verify(accountDAO).updateById(testAccount.getId(), "Name", "Valentin");
    }

    @Test
    public void sendFriendRequestTest() throws DaoException, ServiceException {
        Account friend = new Account(1, "Genadiy", "Zuganov", "gzu@gmail.com");
        accountService.sendFriendRequest(testAccount, friend);
        verify(accountDAO).suggestFriendship(testAccount, friend);
    }

    @Test
    public void sendAcceptRequestTest() throws DaoException, ServiceException {
        Account friend = new Account(1, "Genadiy", "Zuganov", "gzu@gmail.com");
        accountService.acceptFriendRequest(testAccount, friend);
        verify(accountDAO).acceptFriendship(testAccount, friend);
    }

    @Test
    public void declineRequestOrDeleteTest() throws ServiceException, DaoException {
        Account friend = new Account(1, "Genadiy", "Zuganov", "gzu@gmail.com");
        accountService.declineFriendRequest(testAccount, friend);
        verify(accountDAO).declineFriendship(testAccount, friend);
    }

    // TODO: 23.09.2017 Просимулировать логику в сервис методе. Только мокирование недостаточно
    @Test
    public void getFriendsListTest() throws DaoException, ServiceException {
        accountService.getFriendsList(testAccount);
        verify(accountDAO).setFriendsAccounts(testAccount);
    }

    @Test
    public void getFollowedByListTest() throws DaoException, ServiceException {
        accountService.getFollowedByList(testAccount);
        verify(accountDAO).setFollowedAccounts(testAccount);
    }

    @Test
    public void getFollowersListTest() throws DaoException, ServiceException {
        accountService.getFollowersList(testAccount);
        verify(accountDAO).setFollowersAccounts(testAccount);
    }*/
}