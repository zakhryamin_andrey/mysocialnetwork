package com.getjavajob.training.web1705.zakhryamina.service;

/**
 * Created by Andrey's laptop on 29.09.2017. GJJ-Web
 */
public class ServiceException extends Exception {
    public ServiceException(String msg) {
        super(msg);
    }

    public ServiceException(String msg, Throwable e) {
        super(msg, e);
    }
}
