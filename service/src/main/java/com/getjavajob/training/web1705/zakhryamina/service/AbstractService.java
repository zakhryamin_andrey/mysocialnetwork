package com.getjavajob.training.web1705.zakhryamina.service;

import com.getjavajob.training.web1705.zakhryamina.common.DBAccessable;
import com.getjavajob.training.web1705.zakhryamina.dao.AbstractBasicDAO;
import com.getjavajob.training.web1705.zakhryamina.dao.utils.DaoException;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Andrey's laptop on 04.10.2017. GJJ-Web
 */
public abstract class AbstractService <E extends DBAccessable> {
    private AbstractBasicDAO<E> abstractBasicDAO;

    public AbstractService(AbstractBasicDAO<E> abstractBasicDAO) {
        this.abstractBasicDAO = abstractBasicDAO;
    }

    protected AbstractBasicDAO<E> getAbstractBasicDAO() {
        return abstractBasicDAO;
    }

    @Transactional
    public E insertEntry(E entry) throws ServiceException {
        try {
            getAbstractBasicDAO().createInstance(entry);
        } catch (DaoException e) {
            throw new ServiceException("Unable to insert the entry to database");
        }
        return entry;
    }

    @Transactional
    public void deleteEntry(E entry) throws ServiceException {
        try {
            abstractBasicDAO.deleteById(entry);
        } catch (DaoException e) {
            throw new ServiceException("Unable to delete the entry in the database");
        }
    }

    @Transactional
    public void modifyEntry(E newEntry) throws ServiceException {
        try {
            getAbstractBasicDAO().update(newEntry);
        } catch (DaoException e) {
            throw new ServiceException("Unable to update the entry in the database");
        }
    }

    public E getEntryById(int id) throws ServiceException {
        try {
            return getAbstractBasicDAO().selectById(id);
        } catch (DaoException e) {
            throw new ServiceException("Unable to get the entry from the database");
        }
    }

    public List<E> getAllEntries() throws ServiceException {
        try {
            return getAbstractBasicDAO().selectAll();
        } catch (DaoException e) {
            throw new ServiceException("Unable to get all entries in the database");
        }
    }
}