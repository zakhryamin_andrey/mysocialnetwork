package com.getjavajob.training.web1705.zakhryamina.service;

import com.getjavajob.training.web1705.zakhryamina.common.Account;
import com.getjavajob.training.web1705.zakhryamina.common.Group;
import com.getjavajob.training.web1705.zakhryamina.dao.AbstractBasicDAO;
import com.getjavajob.training.web1705.zakhryamina.dao.GroupDAO;
import com.getjavajob.training.web1705.zakhryamina.dao.utils.DaoException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Andrey's laptop on 04.10.2017. GJJ-Web
 */
@Repository
public class GroupService extends AbstractService<Group> {
    @Autowired
    public GroupService(AbstractBasicDAO<Group> abstractBasicDao) {
        super(abstractBasicDao);
    }

    @Override
    protected GroupDAO getAbstractBasicDAO() {
        return (GroupDAO) super.getAbstractBasicDAO();
    }

    public List<Group> getGroupsOfAccount(Account account) {
        return account.getAccountGroups();
    }

    public List<Group> searchGroupsByName(String searchStr) throws ServiceException {
        try {
            List<Group> allGroups = getAbstractBasicDAO().selectAll();
            allGroups.removeIf(group -> (group.getName().toLowerCase().contains(searchStr.toLowerCase())));
            return allGroups;
        } catch (DaoException e) {
            throw new ServiceException("Unable to obtain list of groups", e);
        }
    }
}