package com.getjavajob.training.web1705.zakhryamina.service;

import com.getjavajob.training.web1705.zakhryamina.common.Account;
import com.getjavajob.training.web1705.zakhryamina.common.Group;
import com.getjavajob.training.web1705.zakhryamina.dao.AbstractBasicDAO;
import com.getjavajob.training.web1705.zakhryamina.dao.AccountDAO;
import com.getjavajob.training.web1705.zakhryamina.dao.FriendsDAO;
import com.getjavajob.training.web1705.zakhryamina.dao.utils.DaoException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Andrey's laptop on 19.09.2017. GJJ-Web
 */
@Repository
public class AccountService extends AbstractService<Account> {
    private FriendsDAO friendsDAO;

    @Autowired
    public AccountService(AbstractBasicDAO<Account> abstractBasicDao, FriendsDAO friendsDAO) {
        super(abstractBasicDao);
        this.friendsDAO = friendsDAO;
    }

    @Override
    protected AccountDAO getAbstractBasicDAO() {
        return (AccountDAO) super.getAbstractBasicDAO();
    }

    @Transactional
    public void sendFriendRequest(Account account, Account friend) throws ServiceException {
        Account daoFromAccount = getEntryById(account.getId());
        Account daoToAccount = getEntryById(friend.getId());
        friendsDAO.createFriendRequest(daoFromAccount, daoToAccount);
    }

    @Transactional
    public void acceptFriendRequest(Account account, Account friend) throws ServiceException {
        Account daoFromAccount = getEntryById(account.getId());
        Account daoToAccount = getEntryById(friend.getId());
        friendsDAO.acceptRequest(daoFromAccount, daoToAccount);
    }

    @Transactional
    public void declineFriendRequest(Account account, Account friend) throws ServiceException {
        Account daoFromAccount = getEntryById(account.getId());
        Account daoToAccount = getEntryById(friend.getId());
        friendsDAO.rejectRequest(daoFromAccount, daoToAccount);
    }

    public List<Account> getFriendsList(Account account) throws ServiceException {
        List<Account> accounts = new LinkedList<>();
        accounts.addAll(getEntryById(account.getId()).getFriends());
        return accounts;
    }

    public List<Account> getFollowersList(Account account) throws ServiceException {
        List<Account> accounts = new LinkedList<>();
        accounts.addAll(getEntryById(account.getId()).getFollowers());
        return accounts;
    }

    public List<Account> getFollowedByList(Account account) throws ServiceException {
        List<Account> accounts = new LinkedList<>();
        accounts.addAll(getEntryById(account.getId()).getFollowedByAccount());
        return accounts;
    }

    @Transactional
    public void removeFriend(Account from, Account to) throws ServiceException {
        Account daoFromAccount = getEntryById(from.getId());
        Account daoToAccount = getEntryById(to.getId());
        friendsDAO.removeRequest(daoFromAccount, daoToAccount);
    }

    public Account getAccountByEmail(String email) throws ServiceException {
        try {
            return getAbstractBasicDAO().getAccountByEmail(email);
        } catch (DaoException e) {
            throw new ServiceException("Unable to obtain account by email");
        }
    }

    public List<Account> searchForAccountsByStr(String searchStr) throws ServiceException {
        try {
            List<Account> allAccounts = getAbstractBasicDAO().selectAll();
            allAccounts.removeIf(account -> !(account.getName() + " " + account.getSurname()).toLowerCase().contains(searchStr.toLowerCase()));
            return allAccounts;
        } catch (DaoException e) {
            throw new ServiceException("Unable to obtain list of friends", e);
        }
    }

    public byte[] getAvatarById(int id) throws ServiceException {
        try {
            return getAbstractBasicDAO().getAvatarById(id);
        } catch (DaoException e) {
            throw new ServiceException("Unable to obtain list of friends", e);
        }
    }

    public boolean isLogged(String email, String pass) throws ServiceException {
        if (email == null || pass == null) {
            return false;
        }
        try {
            Account account = getAbstractBasicDAO().getAccountByEmail(email);
            return account != null && account.getPassword().equals(pass);
        } catch (DaoException e) {
            throw new ServiceException("Unable to check is logged or not", e);
        }
    }

    @Transactional
    public List<Group> getGroupsOfAccount(Account account) throws ServiceException {
        List<Group> groups = new LinkedList<>();
        groups.addAll(getEntryById(account.getId()).getAccountGroups());
        return groups;
    }
}