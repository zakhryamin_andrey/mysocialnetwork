DROP TABLE IF EXISTS SOCIAL_NETWORK_DB.PHONES_TBL;
DROP TABLE IF EXISTS SOCIAL_NETWORK_DB.ACCOUNTS_TBL;
DROP TABLE IF EXISTS SOCIAL_NETWORK_DB.MEMBERS_TBL;
DROP TABLE IF EXISTS SOCIAL_NETWORK_DB.GROUPS_TBL;
DROP TABLE IF EXISTS SOCIAL_NETWORK_DB.FRIENDS_TBL;