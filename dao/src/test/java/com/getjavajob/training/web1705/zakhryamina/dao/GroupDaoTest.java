package com.getjavajob.training.web1705.zakhryamina.dao;

import com.getjavajob.training.web1705.zakhryamina.common.Group;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

/**
 * Created by Andrey's laptop on 09.09.2017. GJJ-Web
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:social-dao-context.xml", "classpath:social-test-context-overrides.xml"})
public class GroupDaoTest extends DaoInitializeTest {
    @Autowired
    private GroupDAO groupDao;
    private List<Group> groupDaoTest;

    /*@Before
    public void inintializeTest() throws DaoException {
        try {
            executeSql(getConnection(), CREATE_TABLES_NAME);
            executeSql(getConnection(), INSERT_DATA_TABLES_NAME);
        } catch (SQLException | IOException e) {
            e.printStackTrace();
        }
        this.groupDaoTest = createGroupsForTest();
    }

    @Test
    public void getByIdTest() throws Exception {
        assertEquals(groupDao.selectById(1).getName(), groupDaoTest.get(0).getName());
    }

    @Test(expected = EmptyResultDataAccessException.class)
    public void getByIncorrectIdTest() throws Exception {
        assertNull(groupDao.selectById(5));
    }

    @Test
    public void getAllTest() throws Exception {
        assertEquals(groupDao.selectAll(), groupDaoTest);
    }

    @Test
    public void updateByIdTest() throws Exception {
        groupDaoTest.get(2).setName("Lamers");
        groupDao.updateById(3, "Group_name", groupDaoTest.get(2).getName());
        assertEquals(groupDao.selectAll(), groupDaoTest);
    }

*//*    @Test
    public void updateByEntryTest() throws Exception {
        Group testGroup = new Group(6, "GroupOutOfScope", new Account(20, "Gena", "Zuganov", "genazu@kprf.ru"));
        groupDao.update(testGroup);
        assertEquals(groupDao.selectAll(), groupDaoTest);
    }*//*

    @Test
    public void deleteTest() throws Exception {
        groupDao.deleteById(1);
        groupDaoTest.remove(0);
        assertEquals(groupDao.selectAll(), groupDaoTest);
    }

    @Test
    public void deleteNonExistingTest() throws Exception {
        groupDao.deleteById(4);
        assertEquals(groupDao.selectAll(), groupDaoTest);
    }

    @Test
    public void insertTest() throws Exception {
        Group testGroup = new Group(4, "Testers", new Account(1, "Justin", "Bieber", "justbieber@gmail.com"));
        groupDaoTest.add(testGroup);
        groupDao.insert(testGroup);
        assertEquals(groupDao.selectAll(), groupDaoTest);
    }

*//*    @Test (expected = SQLIntegrityConstraintViolationException.class)
    public void insertAlreadyExistingTest() throws Exception {
        Group testGroup = new Group(2, "Developers", new Account(3, "Vitali", "Severin", "vseverin@gmail.com"));
        groupDaoTest.add(testGroup);
        groupDao.insert(testGroup);
        assertEquals(groupDao.selectAll(), groupDaoTest);
    }*//*

    @After
    public void clearResources() throws Exception {
        super.disconnectAndClear();
//        groupDao.getJDBCTemplate().putConnectionBack();
    }*/
}
