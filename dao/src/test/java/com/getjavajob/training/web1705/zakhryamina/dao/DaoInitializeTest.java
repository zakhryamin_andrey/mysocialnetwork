package com.getjavajob.training.web1705.zakhryamina.dao;

import com.getjavajob.training.web1705.zakhryamina.common.Account;
import com.getjavajob.training.web1705.zakhryamina.common.FriendRequest;
import com.getjavajob.training.web1705.zakhryamina.common.Group;
import com.getjavajob.training.web1705.zakhryamina.dao.utils.SqlUtils;
import org.junit.After;
import org.junit.Before;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Andrey's laptop on 06.09.2017. GJJ-Web
 */
public class DaoInitializeTest {
    private final static String PROPERTIES = "h2_connection.properties";

    @Before
    public void onBefore() throws Exception {
//        SchemaDropper.dropSchema(PROPERTIES);
        SqlUtils.executeSql(PROPERTIES, "create_all_tbl.sql");
        SqlUtils.executeSql(PROPERTIES, "initial_data.sql");
    }

    @After
    public void onAfter() throws Exception {
        SqlUtils.executeSql(PROPERTIES, "schema_dropper.sql");
    }

    protected static List<Account> createAccountsForTest() {
        Account account1 = new Account(1, "Andrey", "Zakhryamin", "azakhryamin@gmail.com");
        try {
            account1.setBirthday(new SimpleDateFormat("yyyy-MM-dd").parse("1990-07-30"));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Account account2 = new Account(2, "Emin", "Mamedov", "mamedov@gmail.com");
        Account account3 = new Account(3, "Vitaly", "Severin", "severin@gmail.com");
        ArrayList<Account> accounts = new ArrayList<>(3);
        accounts.add(account1);
        accounts.add(account2);
        accounts.add(account3);
        return accounts;
    }

    protected static List<Group> createGroupsForTest() {
        Account account1 = createAccountsForTest().get(0);
        Account account2 = createAccountsForTest().get(1);
        Account account3 = createAccountsForTest().get(2);

        ArrayList<Group> groups = new ArrayList<>(3);
        Group firstGroup = new Group(1);
        firstGroup.setName("getJavaJob Init");
        firstGroup.setOwner(account3);
        firstGroup.addMember(account2);
        firstGroup.addMember(account3);
        groups.add(firstGroup);

        Group secondGroup = new Group(2);
        secondGroup.setName("getJavaJob Algo");
        secondGroup.setOwner(account3);
        secondGroup.addMember(account1);
        groups.add(secondGroup);

        Group thirdGroup = new Group(3);
        thirdGroup.setName("getJavaJob Web");
        thirdGroup.setOwner(account2);
        groups.add(thirdGroup);
        return groups;
    }

    protected static List<FriendRequest> createRequestsForTest() {
        Account account1 = createAccountsForTest().get(0);
        Account account2 = createAccountsForTest().get(1);
        Account account3 = createAccountsForTest().get(2);

        List<FriendRequest> requests = new LinkedList<>();
        requests.add(new FriendRequest(account1, account3, 1));
        requests.add(new FriendRequest(account2, account1));
        requests.add(new FriendRequest(account2, account3, 1));
        return requests;
    }
}