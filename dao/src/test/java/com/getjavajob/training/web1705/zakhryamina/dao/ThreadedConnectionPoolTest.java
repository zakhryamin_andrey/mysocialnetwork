package com.getjavajob.training.web1705.zakhryamina.dao;

import com.getjavajob.training.web1705.zakhryamina.dao.utils.DaoException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.sql.Connection;

import static org.junit.Assert.*;


public class ThreadedConnectionPoolTest {
    private ThreadedConnectionPool threadedConnectionPool;

    @Before
    public void initializeTestData() {
        this.threadedConnectionPool = new ThreadedConnectionPool("h2_connection.properties", 3);
    }

    @Test
    public void getActiveTest() throws Exception {
        assertEquals(threadedConnectionPool.getActiveConnections(), 0);
        threadedConnectionPool.getConnection();
        assertEquals(threadedConnectionPool.getActiveConnections(), 1);
        Connection connection = threadedConnectionPool.getConnection();
        assertEquals(threadedConnectionPool.getActiveConnections(), 2);
        threadedConnectionPool.putConnectionBack(connection);
        assertEquals(threadedConnectionPool.getActiveConnections(), 1);
    }

    @Test
    public void getAvailableConnections() throws Exception {
        assertEquals(threadedConnectionPool.getAvailableConnections(), 3);
        threadedConnectionPool.getConnection();
        assertEquals(threadedConnectionPool.getAvailableConnections(), 2);
        Connection connection = threadedConnectionPool.getConnection();
        threadedConnectionPool.putConnectionBack(connection);
        assertEquals(threadedConnectionPool.getAvailableConnections(), 2);
    }

    @Test (expected = DaoException.class)
    public void isAlreadyClosedTest() throws Exception {
        assertFalse(threadedConnectionPool.isAlreadyClosed());
        Connection connection = threadedConnectionPool.getConnection();
        threadedConnectionPool.close();
        assertFalse(threadedConnectionPool.isAlreadyClosed());
        threadedConnectionPool.putConnectionBack(connection);
        assertTrue(threadedConnectionPool.isAlreadyClosed());
        assertEquals(threadedConnectionPool.getAvailableConnections(), 3);
        assertEquals(threadedConnectionPool.getActiveConnections(), 0);
        threadedConnectionPool.getConnection();
    }

    @Test(expected = DaoException.class)
    public void isMakeAvailableTest() throws Exception {
        assertFalse(threadedConnectionPool.isClosing());
        Connection connection = threadedConnectionPool.getConnection();
        threadedConnectionPool.close();
        assertFalse(threadedConnectionPool.isAlreadyClosed());
        threadedConnectionPool.putConnectionBack(connection);
        assertTrue(threadedConnectionPool.isAlreadyClosed());
        assertEquals(threadedConnectionPool.getAvailableConnections(), 3);
        assertEquals(threadedConnectionPool.getActiveConnections(), 0);
        threadedConnectionPool.getConnection();
        assertEquals(threadedConnectionPool.getAvailableConnections(), 3);
        assertEquals(threadedConnectionPool.getActiveConnections(), 0);
        threadedConnectionPool.setAvailable();
        threadedConnectionPool.getConnection();
        assertEquals(threadedConnectionPool.getAvailableConnections(), 2);
        assertEquals(threadedConnectionPool.getActiveConnections(), 1);
    }

    @After
    public void clearResources() throws Exception {
        threadedConnectionPool.close();
    }
}