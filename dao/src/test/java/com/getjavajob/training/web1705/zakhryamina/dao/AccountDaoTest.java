package com.getjavajob.training.web1705.zakhryamina.dao;

import com.getjavajob.training.web1705.zakhryamina.common.Account;
import com.getjavajob.training.web1705.zakhryamina.common.Group;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by Andrey's laptop on 09.09.2017. GJJ-Web
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:social-dao-context.xml", "classpath:social-test-context-overrides.xml"})
public class AccountDaoTest extends DaoInitializeTest {
    @Autowired
    private AccountDAO dao;
    private List<Account> accounts;

    @Override
    public void onBefore() throws Exception {
        super.onBefore();
        accounts = createAccountsForTest();
    }

    @Transactional
    @Test
    public void getEntityByIdTest() throws Exception {
        Account account = dao.selectById(1);
        assertEquals(account, accounts.get(0));
        account = dao.selectById(2);
        assertEquals(account, accounts.get(1));
        account = dao.selectById(3);
        assertEquals(account, accounts.get(2));
    }

    @Transactional
    @Test
    public void getEntityByNonExistentIdTest() throws Exception {
        assertNull(dao.selectById(15));
    }

    @Transactional
    @Test
    public void getAllTest() throws Exception {
        List<Account> daoAccounts = dao.selectAll();
        assertEquals(daoAccounts.get(0), accounts.get(0));
        assertEquals(daoAccounts.get(1), accounts.get(1));
        assertEquals(daoAccounts.get(2), accounts.get(2));
    }

    @Transactional
    @Test
    public void updateTest() throws Exception {
        accounts.get(2).setEmail("new_email@gmail.com");
        dao.update(accounts.get(2));
        assertEquals(dao.selectById(3), accounts.get(2));
    }

    @Transactional
    @Test
    public void createTest() throws Exception {
        Account account = new Account("Valentin", "Smirnov", "smirnovv@mail.ru", "qwerty");
        accounts.add(dao.createInstance(account));
        assertEquals(dao.selectById(4), accounts.get(3));
    }

    @Transactional
    @Test
    public void getAccountByEmailTest() throws Exception {
        assertEquals(dao.getAccountByEmail("mamedov@gmail.com"), accounts.get(1));
    }

    @Transactional
    @Test
    public void getAccountByNonExistentEmailTest() throws Exception {
        assertNull(dao.getAccountByEmail("mamedovE@gmail.com"));
    }

    @Transactional
    @Test
    public void friendsConnectionsTest() throws Exception {
        assertTrue(dao.selectById(1).getFriends().contains(accounts.get(2)));
    }

    @Transactional
    @Test
    public void followersConnectionsTest() throws Exception {
        assertTrue(dao.selectById(1).getFollowers().contains(accounts.get(1)));
    }

    @Transactional
    @Test
    public void followedConnectionsTest() throws Exception {
        System.out.println(Arrays.toString(dao.selectById(2).getFollowedByAccount().toArray()));
        assertTrue(dao.selectById(2).getFollowedByAccount().contains(accounts.get(0)));
    }

    @Transactional
    @Test
    public void getSkillsTest() throws Exception {
        /*List<String> list = dao.s(1).getSkills();
        assertTrue(list.contains("JavaScript"));
        assertTrue(list.contains("jQuery"));
        assertTrue(list.contains("Spring Framework"));
        assertTrue(list.contains("Java Core"));*/
    }

    @Transactional
    @Test
    public void getGroupsOfAccountTest() throws Exception {
        List<Group> testGroups = dao.selectById(3).getAccountGroups();
        assertTrue(testGroups.contains(createGroupsForTest().get(0)));
        assertTrue(testGroups.contains(createGroupsForTest().get(1)));
        assertFalse(testGroups.contains(createGroupsForTest().get(2)));
    }
}