package com.getjavajob.training.web1705.zakhryamina.dao;

import com.getjavajob.training.web1705.zakhryamina.dao.utils.DaoException;

import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.Properties;
import java.util.Queue;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by Andrey's laptop on 04.10.2017. GJJ-Web
 */
public class ThreadedConnectionPool implements AutoCloseable, Pool {
    private final int DEFAULT_POOL_SIZE = 5;
    private final String DEFAULT_DATA_SOURCE = "db_connection_prop.properties";

    private Lock lock;
    private Condition lockCondition;
    private boolean closingFlag;
    private Properties properties;
    private Queue<Connection> connections;
    private int poolSize;
    private int activeConnections;

    public ThreadedConnectionPool() {
        this.lock = new ReentrantLock();
        this.lockCondition = lock.newCondition();
        this.properties = new Properties();
        this.poolSize = DEFAULT_POOL_SIZE;
        this.connections = new LinkedList<>();
        try {
            properties.load(new InputStreamReader(ThreadedConnectionPool.class.getClassLoader().getResourceAsStream(DEFAULT_DATA_SOURCE)));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public ThreadedConnectionPool(String sourceFile, int poolSize) {
        this();
        this.poolSize = poolSize;
        try {
            properties.load(new InputStreamReader(ThreadedConnectionPool.class.getClassLoader().getResourceAsStream(sourceFile)));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Connection getConnection() throws DaoException {
        try {
            lock.lock();
            if (activeConnections >= poolSize) {
                lockCondition.await();
            }
            if (!closingFlag) {
                if (connections.isEmpty()) {
//                    connections.add(createConnection(properties));
                }
            } else {
                throw new DaoException("Unable to obtain new connection while pool is closing");
            }
            activeConnections++;
            return connections.poll();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            lock.unlock();
        }
        return null;
    }

    @Override
    public void putConnectionBack(Connection freeConnection) throws DaoException {
        try {
            lock.lock();
            if (!closingFlag) {
                connections.add(freeConnection);
            } else {
                freeConnection.close();
            }
            if (activeConnections >= poolSize) {
                lockCondition.signal();
            }
            activeConnections--;
        } catch (SQLException e) {
            throw new DaoException("Unable to close the connection");
        } finally {
            lock.unlock();
        }
    }

    public int getAvailableConnections() {
        return poolSize - activeConnections;
    }

    @Override
    public int getActiveConnections() {
        return activeConnections;
    }

    public boolean isAlreadyClosed() {
        return activeConnections == 0 && closingFlag;
    }

    public int getConnectionsSize() {
        return connections.size();
    }

    public boolean isClosing() {
        return closingFlag;
    }

    public void setAvailable() {
        closingFlag = false;
    }

    public void putAllConnectionsBack() {
        try {
            lock.lock();
            while (!connections.isEmpty()) {
                putConnectionBack(connections.poll());
            }
        } catch (DaoException daoException) {
            daoException.printStackTrace();
        } finally {
            lock.unlock();
        }
    }

    @Override
    public void close() {
        closingFlag = true;
        if (!isAlreadyClosed()) {
            putAllConnectionsBack();
        }
        if (activeConnections <= 0) {
            connections.clear();
        }
    }
}
