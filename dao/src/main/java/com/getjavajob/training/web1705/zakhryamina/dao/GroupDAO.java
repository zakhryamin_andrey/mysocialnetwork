package com.getjavajob.training.web1705.zakhryamina.dao;

import com.getjavajob.training.web1705.zakhryamina.common.Group;
import com.getjavajob.training.web1705.zakhryamina.dao.utils.DaoException;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 * Created by Andrey's laptop on 04.10.2017. GJJ-Web
 */
@Repository
public class GroupDAO extends AbstractBasicDAO<Group> {
    @Override
    protected CriteriaQuery<Group> getSelectAllQuery() {
        CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<Group> query = builder.createQuery(Group.class);
        Root<Group> groupRoot = query.from(Group.class);
        query.select(groupRoot);
        return query;
    }

    @Override
    protected CriteriaQuery<Group> getSelectByIdQuery(int id) {
        CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<Group> query = builder.createQuery(Group.class);
        Root<Group> groupRoot = query.from(Group.class);
        query.select(groupRoot).where(builder.equal(groupRoot.get("id"), id));
        return query;
    }

    @Override
    public void update(Group group) throws DaoException {
        super.update(group);
        Group updateGroup = selectById(group.getId());
        updateGroup.setOwner(group.getOwner());
        updateGroup.setName(group.getName());
        updateGroup.setActiveStatus(group.isActiveStatus());
        updateGroup.setSupplementInfo(group.getSupplementInfo());
        updateGroup.setMembers(group.getMembers());
        updateGroup.setAvatar(group.getAvatar());
    }
}