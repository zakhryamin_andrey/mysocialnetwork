package com.getjavajob.training.web1705.zakhryamina.dao;

import com.getjavajob.training.web1705.zakhryamina.common.Account;
import com.getjavajob.training.web1705.zakhryamina.common.FriendRequest;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

/**
 * Created by Andrey's laptop on 30.11.2017. GJJ-Web
 */
@Repository
public class FriendsDAO {
    @PersistenceContext
    private EntityManager entityManager;

    public void createFriendRequest(Account from, Account to) {
        FriendRequest request = new FriendRequest(from, to);
        entityManager.merge(request);
//        entityManager.persist(request);
    }

    public void createFriendRequest(FriendRequest friendRequest) {
        entityManager.persist(friendRequest);
    }

    public List<FriendRequest> getAllRequests() {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<FriendRequest> query = builder.createQuery(FriendRequest.class);
        Root<FriendRequest> requestRoot = query.from(FriendRequest.class);
        query.select(requestRoot);
        return entityManager.createQuery(query).getResultList();
    }

    public FriendRequest getFriendRequest(Account from, Account to) {
        try {
            CriteriaBuilder builder = entityManager.getCriteriaBuilder();
            CriteriaQuery<FriendRequest> query = builder.createQuery(FriendRequest.class);
            Root<FriendRequest> requestRoot = query.from(FriendRequest.class);
            query.select(requestRoot).where(
                    builder.and(
                            builder.equal(requestRoot.get("fromAccount"), from.getId()),
                            builder.equal(requestRoot.get("toAccount"), to.getId())
                    ));
            return entityManager.createQuery(query).getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    public List<FriendRequest> getAllAccountRequests(Account account) {
        long accId = account.getId();
        return entityManager.createNamedQuery("findAllAccountRequests", FriendRequest.class)
                .setParameter("from", accId)
                .setParameter("to", accId).getResultList();
    }

    public void acceptRequest(Account from, Account to) {
        getFriendRequest(from, to).setStatus(1);
    }

    public void rejectRequest(Account from, Account to) {
        getFriendRequest(from, to).setStatus(2);
    }

    public void removeRequest(Account from, Account to) {
        entityManager.remove(getFriendRequest(from, to));
    }

//    ///////////////////////////////////////////////////////////////
}
