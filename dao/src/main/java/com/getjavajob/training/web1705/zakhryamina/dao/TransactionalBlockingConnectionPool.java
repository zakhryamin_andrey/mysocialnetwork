package com.getjavajob.training.web1705.zakhryamina.dao;

import com.getjavajob.training.web1705.zakhryamina.dao.utils.DaoException;

import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;
import java.util.concurrent.Semaphore;

/**
 * Created by Andrey's laptop on 25.10.2017. GJJ-Web
 */
public class TransactionalBlockingConnectionPool implements Pool {
    private static final String DEFAULT_DATA_SOURCE = "db_connection_prop.properties";
    private static final int DEFAULT_SIZE = 20;

    private Properties prop;
    private Semaphore semaphore;
    private ThreadLocal<Connection> threadLocal;
    private int size;

    public TransactionalBlockingConnectionPool() {
        this.size = DEFAULT_SIZE;
        this.semaphore = new Semaphore(DEFAULT_SIZE);
        this.threadLocal = new ThreadLocal<>();
        this.prop = new Properties();
        try {
            this.prop.load(new InputStreamReader(TransactionalBlockingConnectionPool.class.getClassLoader().getResourceAsStream(DEFAULT_DATA_SOURCE)));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public TransactionalBlockingConnectionPool(int size, String propertiesFilePath) {
        this.size = size;
        this.semaphore = new Semaphore(size);
        this.threadLocal = new ThreadLocal<>();
        this.prop = new Properties();
        try {
            this.prop.load(new InputStreamReader(ThreadedConnectionPool.class.getClassLoader().getResourceAsStream(propertiesFilePath)));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Connection getConnection() throws DaoException {
        try {
            semaphore.acquire();
            Connection connection = threadLocal.get();
            threadLocal.remove();
            if (connection == null) {
//                connection = createConnection(prop);
                connection.setAutoCommit(true);
            }
            return connection;
        } catch (InterruptedException e) {
            throw new DaoException("Concurrent exception while acquiring connection", e);
        } catch (SQLException e) {
            throw new DaoException("Unable to set Auto Commit", e);
        }
    }

    @Override
    public void putConnectionBack(Connection connectionBack) throws DaoException {
        threadLocal.set(connectionBack);
        semaphore.release();
    }

    public int getFreeConnections() {
        return semaphore.availablePermits();
    }

    public void commitConnection() throws DaoException {
        Connection connection = threadLocal.get();
        try {
            connection.commit();
        } catch (SQLException e) {
            throw new DaoException("Unable to commit connection", e);
        } finally {
            closeConnection(connection);
        }
    }

    public void rollBackConnection() throws DaoException {
        Connection connection = threadLocal.get();
        try {
            connection.rollback();
        } catch (SQLException | NullPointerException e) {
            throw new DaoException("Unable to rollBack connection", e);
        } finally {
            closeConnection(connection);
        }
    }

    @Override
    public int getActiveConnections() {
        return size - semaphore.availablePermits();
    }

    private void closeConnection(Connection connection) throws DaoException {
        try {
            connection.close();
        } catch (SQLException e) {
            throw new DaoException("Unable to close connection", e);
        } finally {
            threadLocal.remove();
            semaphore.release();
        }
    }
}