package com.getjavajob.training.web1705.zakhryamina.dao;

import com.getjavajob.training.web1705.zakhryamina.common.DBAccessable;
import com.getjavajob.training.web1705.zakhryamina.dao.utils.DaoException;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.criteria.CriteriaQuery;
import java.util.List;

/**
 * Created by Andrey's laptop on 04.10.2017. GJJ-Web
 */
public abstract class AbstractBasicDAO<E extends DBAccessable> {
    @PersistenceContext
    private EntityManager templateDB;

    protected EntityManager getEntityManager() {
        return templateDB;
    }

    protected abstract CriteriaQuery<E> getSelectAllQuery();

    protected abstract CriteriaQuery<E> getSelectByIdQuery(int id);

    public E selectById(int id) throws DaoException {
        if (id <= 0) {
            return null;
        }
        try {
            CriteriaQuery<E> query = getSelectByIdQuery(id);
            return getEntityManager().createQuery(query).getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    public E createInstance(E entry) throws DaoException {
        if (entry == null) {
            return null;
        }
        try {
            return getEntityManager().merge(entry);
        } catch (PersistenceException e) {
            throw new DaoException("Not null field on inserted object is null.", e);
        }
    }

    public List<E> selectAll() throws DaoException {
        CriteriaQuery<E> query = getSelectAllQuery();
        return getEntityManager().createQuery(query).getResultList();
    }

    public void deleteById(E entry) throws DaoException {
        if (entry == null) {
            return;
        }
        getEntityManager().remove(entry);
    }

    public void update(E entry) throws DaoException {
        if (entry == null) {
            return;
        }
    }
}