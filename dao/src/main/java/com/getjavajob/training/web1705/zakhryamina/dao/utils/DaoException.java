package com.getjavajob.training.web1705.zakhryamina.dao.utils;

/**
 * Created by Andrey's laptop on 12.09.2017. GJJ-Web
 */
public class DaoException extends Exception {
    public DaoException(String msg) {
        super(msg);
    }

    public DaoException(String msg, Throwable cause) {
        super(msg, cause);
    }
}
