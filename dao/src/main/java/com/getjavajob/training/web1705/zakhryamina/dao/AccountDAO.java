package com.getjavajob.training.web1705.zakhryamina.dao;

import com.getjavajob.training.web1705.zakhryamina.common.Account;
import com.getjavajob.training.web1705.zakhryamina.dao.utils.DaoException;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 * Created by Andrey's laptop on 04.10.2017. GJJ-Web
 */
@Repository
public class AccountDAO extends AbstractBasicDAO<Account> {
    @Override
    protected CriteriaQuery<Account> getSelectAllQuery() {
        CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<Account> query = builder.createQuery(Account.class);
        Root<Account> accountRoot = query.from(Account.class);
        query.select(accountRoot);
        return query;
    }

    @Override
    protected CriteriaQuery<Account> getSelectByIdQuery(int id) {
        CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<Account> query = builder.createQuery(Account.class);
        Root<Account> accountRoot = query.from(Account.class);
        query.select(accountRoot).where(builder.equal(accountRoot.get("id"), id));
        return query;
    }

    public Account getAccountByEmail(String email) throws DaoException {
        if (email == null) {
            return null;
        }
        try {
            CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
            CriteriaQuery<Account> query = builder.createQuery(Account.class);
            Root<Account> accountRoot = query.from(Account.class);
            query.select(accountRoot).where(builder.equal(accountRoot.get("email"), email));
            return getEntityManager().createQuery(query).getSingleResult();
        } catch (NoResultException e) {
            throw new DaoException("Unable to get the entry by id");
        }
    }

    public Account getAccountById(int id) throws DaoException {
        if (id <= 0) {
            return null;
        }
        try {
            CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
            CriteriaQuery<Account> query = builder.createQuery(Account.class);
            Root<Account> accountRoot = query.from(Account.class);
            query.select(accountRoot).where(builder.equal(accountRoot.get("id"), id));
            return getEntityManager().createQuery(query).getSingleResult();
        } catch (NoResultException e) {
            throw new DaoException("Unable to get the entry by id");
        }
    }

    public byte[] getAvatarById(int id) throws DaoException {
        if (id <= 0) {
            return null;
        }
        try {
            return getAccountById(id).getAvatar();
        } catch (DaoException e) {
            throw new DaoException("Unable to get the avatar by id");
        }
    }

    @Override
    public void update(Account account) throws DaoException {
        super.update(account);
        Account updateAccount = selectById(account.getId());
        updateAccount.setName(account.getName());
        updateAccount.setSurname(account.getSurname());
        updateAccount.setMiddleName(account.getMiddleName());
        updateAccount.setBirthday(account.getBirthday());
        updateAccount.setPersonalPhones(account.getPersonalPhones());
        updateAccount.setPassword(account.getPassword());
        updateAccount.setWorkPhones(account.getWorkPhones());
        updateAccount.setEmail(account.getEmail());
        updateAccount.setIcq(account.getIcq());
        updateAccount.setSkype(account.getSkype());
        updateAccount.setSupplementInfo(account.getSupplementInfo());
        updateAccount.setActiveStatus(account.isActiveStatus());
        updateAccount.setAccountGroups(account.getAccountGroups());
        updateAccount.setAvatar(account.getAvatar());
    }
}