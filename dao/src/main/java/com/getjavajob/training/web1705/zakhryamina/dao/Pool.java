package com.getjavajob.training.web1705.zakhryamina.dao;

import com.getjavajob.training.web1705.zakhryamina.dao.utils.DaoException;

import java.sql.Connection;

/**
 * Created by Andrey's laptop on 18.10.2017. GJJ-Web
 */
public interface Pool {
    void putConnectionBack(Connection connection) throws DaoException;

    Connection getConnection() throws DaoException;

    int getActiveConnections();
}