package com.getjavajob.training.web1705.zakhryamina.dao.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

/**
 * Created by Andrey's laptop on 03.10.2017. GJJ-Web
 */
public class SqlUtils {
    private static final String SCHEMA = "SOCIAL_NETWORK_DB";

    public static void executeSql(String propertiesFile, String queriesFileName) throws SQLException, IOException, DaoException {

        Connection connection = createConnection(propertiesFile);
        try (BufferedReader bf = new BufferedReader(new InputStreamReader(
                ClassLoader.getSystemResourceAsStream(queriesFileName)))) {
            StringBuilder sb = new StringBuilder();
            while (bf.ready()) {
                sb.append(bf.readLine().trim());
                if (sb.toString().endsWith(";")) {
                    System.out.println(sb.toString());
                    connection.createStatement().execute(sb.toString());
                    sb = new StringBuilder();
                }
            }
        }
    }

    public static Connection createConnection(String propertiesFile) throws DaoException {
        Properties properties = new Properties();
        try {
            properties.load(SqlUtils.class.getClassLoader().getResourceAsStream(propertiesFile));
            String url = properties.getProperty("database.url");
            String user = properties.getProperty("database.user");
            String password = properties.getProperty("database.password");
            Connection connection = DriverManager.getConnection(url, user, password);
            createAndUseSchema(SCHEMA, connection);
            return connection;
        } catch (SQLException e) {
            throw new DaoException("Unable to create connection, check the settings file", e);
        } catch (IOException e) {
            throw new DaoException("Unable to read properties file", e);
        }
    }

    private static void createAndUseSchema(String schema, Connection connection) {
        try {
            connection.createStatement().execute("USE " + schema);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
