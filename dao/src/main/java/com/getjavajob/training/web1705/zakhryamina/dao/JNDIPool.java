package com.getjavajob.training.web1705.zakhryamina.dao;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by Andrey's laptop on 02.11.2017. GJJ-Web
 */
public class JNDIPool implements Pool {
    private static volatile JNDIPool singletonInstance;
    private DataSource dataSource;
    private ThreadLocal<Connection> threadLocal;
    private ThreadLocal<Integer> connectionsAlreadyInUse;
    private AtomicInteger numOfThreads;

    private JNDIPool() {
        try {
            Context initContext = new InitialContext();
            Context envContext = (Context) initContext.lookup("java:/comp/env");
            this.dataSource = (DataSource) envContext.lookup("jdbc/socialNetwork");
            this.threadLocal = new ThreadLocal<>();
            this.connectionsAlreadyInUse = new ThreadLocal<>();
            this.numOfThreads = new AtomicInteger(0);
        } catch (NamingException e) {
            e.printStackTrace();
        }
    }

    public static JNDIPool getSingletonInstance() {
        if (singletonInstance == null) {
            synchronized (JNDIPool.class) {
                if (singletonInstance == null) {
                    singletonInstance = new JNDIPool();
                }
            }
        }
        return singletonInstance;
    }

    private static void createAndUseSchema(String schema, Connection connection) {
        try {
            connection.createStatement().execute("USE " + schema);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void putConnectionBack(Connection connection) {
        try {
            if (connectionsAlreadyInUse.get() <= 1) {
                connection.close();
                threadLocal.remove();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            connectionsAlreadyInUse.set(numOfThreads.decrementAndGet());
        }
    }

    @Override
    public int getActiveConnections() {
        return connectionsAlreadyInUse.get();
    }

    @Override
    public Connection getConnection() {
        Connection connection = threadLocal.get();
        if (connection == null) {
            try {
                connection = dataSource.getConnection();
                threadLocal.set(connection);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } else {
        }
        connectionsAlreadyInUse.set(numOfThreads.incrementAndGet());
        //            createAndUseSchema(AbstractBasicDAO.SCHEMA, connection);
        return connection;
    }
}