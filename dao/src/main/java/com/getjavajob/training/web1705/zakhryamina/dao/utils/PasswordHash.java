package com.getjavajob.training.web1705.zakhryamina.dao.utils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Andrey's laptop on 06.10.2017. GJJ-Web
 */
public class PasswordHash {
    private Map DB = new HashMap();
    private static final String SALT = "my-salt-text";

    public static void main(String args[]) {
        PasswordHash demo = new PasswordHash();
        demo.signup("john", "dummy123");
        if (demo.login("john", "dummy123"))
            System.out.println("user login successfull.");
        if (demo.login("john", "blahblah"))
            System.out.println("User login successfull.");
        else
            System.out.println("user login failed.");
    }

    public void signup(String username, String password) {
        String saltedPassword = SALT + password;
        String hashedPassword = generateHash(saltedPassword);
        DB.put(username, hashedPassword);
    }

    public Boolean login(String username, String password) {
        Boolean isAuthenticated;
        String saltedPassword = SALT + password;
        String hashedPassword = generateHash(saltedPassword);
        String storedPasswordHash = (String) DB.get(username);
        isAuthenticated = hashedPassword.equals(storedPasswordHash);
        return isAuthenticated;
    }

    public static String generateHash(String input) {
        StringBuilder hash = new StringBuilder();
        try {
            MessageDigest sha = MessageDigest.getInstance("SHA-1");
            byte[] hashedBytes = sha.digest(input.getBytes());
            char[] digits = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
                    'a', 'b', 'c', 'd', 'e', 'f'};
            for (byte b : hashedBytes) {
                hash.append(digits[(b & 0xf0) >> 4]);
                hash.append(digits[b & 0x0f]);
            }
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return hash.toString();
    }
}